const fs = require("fs")

fs.unlink(__dirname + "/../src/assets/images/gallery/.DS_Store", function (err) {
    console.log(err)
})
fs.unlink(__dirname + "/../src/assets/videos/.DS_Store", function (err) {
    console.log(err)
})

let images = fs.readdirSync(__dirname + "/../src/assets/images/gallery")
images = JSON.stringify(images)
console.log("Images: \n"+images)

let videos = fs.readdirSync(__dirname + "/../src/assets/videos")
videos = JSON.stringify(videos)
console.log("Videos: \n"+videos)

let data = "export default function () {\n" +
    "    return {\n" +
    "        disabled: true,\n" +
    "        id: 1,\n" +
    "        name: \"Галерея\",\n" +
    "        desc: \"\",\n" +
    "        images:"+images+",\n" +
    "        videos:"+videos+"\n" +
    "   }" +
    "}"

fs.writeFile(__dirname+'/../src/reducers/gallery.js', data, function(err, data){
    if (err) console.log(err);
    console.log("Successfully Written to File.");
});
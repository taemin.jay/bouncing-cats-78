const express = require('express')
const app = express()
const http = require('http')
const cors = require('cors')
const easyvk = require("easyvk")
const bodyParser = require('body-parser')
const nodemailer = require('nodemailer')
const path = require('path')

const httpServer = http.createServer(app);

app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine())


app.use(require('prerender-node').set('prerenderToken', '7QhOElmlazIBVXTF5aM8N'));
app.use(express.static(__dirname + '/build/static/css'));
app.use(express.static(__dirname + '/build/static/js'));
app.use(express.static(__dirname + '/build/'));
app.use(express.static(__dirname + '/build/static/images'));
app.use(express.static(__dirname + '/build/static/videos'));
app.use(express.static(__dirname + '/build/static/fonts'));
app.use('/taemin.jay/bouncing-cats-78/static/js/', express.static(path.join(__dirname, '/build/static/js')));
app.use('/taemin.jay/bouncing-cats-78/static/css/', express.static(path.join(__dirname, '/build/static/css')));
app.use('/taemin.jay/bouncing-cats-78/', express.static(__dirname + '/build/'));


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get('*', function(req,res) {
    res.sendFile(path.join(__dirname + '/build/index.html'));
})

app.post('/api/email', (req, res) => {
    console.log(req.body)
    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: '465',
        secure: true,
        auth: {
            user: 'bouncingcats78@gmail.com',
            pass: 'Qwerty12#'
        }
    });

    let template_email = `
    <h1>НОВАЯ ЗАЯВКА!</h1></br></br>
    <p style="background: grey; color: white; padding: 10px">Имя: ${req.body.name}</p>
    Телефон:<h2>${req.body.telephone}</h2>
    Запись:<h2>${req.body.additional}</h2>
    Почта:<h2>${req.body.email}</h2>
    Заголовок:<h2>${req.body.title}</h2>
    Сообщение:<h2>${req.body.message}</h2>`

    let mailOptions = {
        from: 'bouncingcats78@gmail.com',
        to: 'bouncingcats78@gmail.com',
        subject: 'Запись',
        html: template_email
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            res
                .status(500)
                .send(error)
                .end();
        } else {
            console.log('Email sent: ' + info.response);
            res
                .status(250)
                .send({status: "Спасибо за заявку!\nНаш Администратор свяжется с вами."})
                .end()
        }
    })

    let template_vk = `
    ____________________________
    ✉НОВАЯ ЗАЯВКА!
    ✉САЙТ\n\n
    🎫Имя:\n${req.body.name}\n\n
    ☎Телефон:\n${req.body.telephone}\n\n
    Запись:\n${req.body.additional}\n\n
    Почта:\n${req.body.email}\n\n
    Заголовок сообщения: ${req.body.title}
    Сообщение: ${req.body.message}`

    easyvk(
        {
            access_token: '898e1fe3a013447eab9841188afd3057ebb1a453df72924e552843fdfc7e079db9d36f5b024e16686a502'
        }
    ).then(vk =>
        {
            console.log(vk.session.group_id);
            vk.call('messages.send', {
                message: template_vk,
                user_id: [101652809]
            }).then(({vkr}) => {})

        }
    ).catch(console.error)
})


httpServer.listen(3000);

import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './assets/css/index.css'
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Page from './components/Pages/Page';
import Registration from './actions/Feedback/Registration'

import {createStore} from 'redux'
import allReducers from "./reducers"
import {Provider} from 'react-redux'

const store = createStore (allReducers);

class App extends Component {
    render() {
        return (
            <div className="App">
                <Provider store={store}>
                    <Registration />
                    <Header />
                    <Page />
                    <Footer />
                </Provider>
            </div>
        );
    }
}

export default App;
import React, {Component} from 'react';
import {connect} from 'react-redux';
import Input from './Input'
import axios from 'axios'


class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            telephone: "",
            email: "",
            title: "",
            message: "",
            additional: "",
            err_Message: false,
            suc_Message: false,
            suc_Message_text: ""
        };
    }

     SendEmail = async (e) => {

        e.preventDefault()

         console.log(this.state)

         if (this.state.name_Enable && this.state.telephone_Enable) {
            await this.setState(
                {
                    err_Message: false
                }
            )
         } else {
            return await this.setState(
                 {
                     err_Message: true
                 }
             )
         }

        let data = {
            name: this.state.name,
            telephone: this.state.telephone,
            email: this.state.email,
            title: this.state.title,
            message: this.state.message,
            additional: this.state.additional
        }

        await axios.post(`${this.props.schoolinfo.url}api/email`, data)
            .then(resolve => {
                console.log(resolve)
                this.setState({
                    suc_Message_text: resolve.data.status,
                    suc_Message: true
                })
            })
            .catch(reject => {
                console.log(reject)
                this.setState({
                    suc_Message_text: toString(reject),
                    suc_Message: true
                })
            })

    }

    buttonName (page) {
        return this.props.feedback.feedbacks.find( feedback => feedback.page === page).button
    }

    getInputValue = async (field, value) => {
        console.log(value)
        await this.setState({
            [field]: value,
            [field+"_Enable"]: true
        })
        console.log(this.state)
    }

    disableBtn = async (field) => {
        await this.setState({
            [field+"_Enable"]: false
        })
    }

    showList (page) {

        let array_forms_default = this.props.feedback.forms.filter( form => form.disabled),
            array_forms_page = this.props.feedback.feedbacks.find( feedback => feedback.page === page).forms,
            array = array_forms_default.concat(array_forms_page),
            tech1 = [], tech2 = []

        array.forEach( (form,index) => {
            if (page === "contact" && (form.field === "telephone" || form.field === "name")) {
                tech1.push(<Input key={index} field={form.field} label={form.label} getInput={this.getInputValue} disableBtn={this.disableBtn}/>)
            } else {
                tech2.push(<Input key={index} field={form.field} label={form.label} getInput={this.getInputValue} disableBtn={this.disableBtn}/>)
            }
            return false
        })

        if (tech1.length > 0) {
            return (
                <form onSubmit={this.SendEmail}>
                    <div className="contact-form d-flex flex-row flex-wrap justify-content-between">
                        {tech1}
                    </div>
                    {tech2}
                    <div className="registration">
                        <button className={"draw in-block"} type="submit">{this.buttonName(this.props.page)}</button>
                    </div>
                </form>
            )
        } else {
            return (
                <form onSubmit={this.SendEmail}>
                    {tech2}
                    <div className="registration">
                        <button className={"draw in-block"} type="submit">{this.buttonName(this.props.page)}</button>
                    </div>
                </form>
            )
        }
    }
    showTitle (page) {
        let feedback = this.props.feedback.feedbacks.find( feedback => feedback.page === page)
        return (
            <div>
                <h2>{feedback.name}</h2>
                <p>{feedback.desc}</p>
                {this.showAddit()}
            </div>
        )
    }

    showAddit () {
        if (this.props.feedback.additional) {
            let add = this.props.feedback.additional,
                name = add.name,
                day = add.day,
                time = add.time
            return (<p className="reg-info">{name}, {day} в {time}</p>)
        }
    }

    render() {
        return (
            <div className="w-100">
                {this.showTitle(this.props.page)}
                {this.showList(this.props.page)}
                {
                    this.state.err_Message ? <p>Не корректно заполнены поля</p> : null
                }
                {
                    this.state.suc_Message ? <p>{this.state.suc_Message_text}</p> : null
                }
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        feedback: state.feedback,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(Form);
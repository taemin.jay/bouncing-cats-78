import React, {Component} from 'react';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            focus: false,
            filled: false,
            error: false
        };
    }

    handleChange = (e) => {

        let value = e.target.value

        console.log(value)

            this.setState(
            {
                value: value
            }
        );

        value.length > 0 ? this.setState({filled: true}) : this.setState({filled: false})

        this.checkValue(this.props.field, value)

    }

    checkValue(field, value) {

        const re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        if (field === 'telephone' && (18 < value.length || value.length < 11)) {
            this.showError()
            this.props.disableBtn(field)
        } else if (field === 'email' && !re_email.test(String(value).toLowerCase())) {
            this.showError()
            this.props.disableBtn(field)
        } else if (field === 'name' && (value.length === 0)) {
            this.showError()
            this.props.disableBtn(field)
        } else {
            this.hideError()

            this.props.getInput(field,value)
        }

    }

    showError() {
        this.setState({
            error: true
        })
    }

    hideError() {
        this.setState({
            error: false
        })
    }

    focusLabel = (e) => {

        this.setState({ focus: true });

    }

    unfocuseLabel = () => {

        this.setState({focus: false});

    }

    render() {
        return (
             <div className="position-relative input">
                <label
                    type="text"
                    htmlFor={this.props.field}
                    className={(this.state.focus || this.state.filled) && !this.state.error ? 'focus-label' : this.state.filled && this.state.error ? 'focus-label-error' : this.state.focus || this.state.filled ? 'focus-label' : null  }
                >
                    {this.props.label}
                </label>
                <input
                    required
                    id={this.props.field}
                    name={this.props.field}
                    onChange={this.handleChange}
                    onFocus={this.focusLabel}
                    onBlur={this.unfocuseLabel}
                />
                 <div
                     className={(this.state.focus || this.state.filled) && !this.state.error ? 'line line-focus' : this.state.filled && this.state.error ? 'line-error line-focus-error' : this.state.focus || this.state.filled ? 'line line-focus' : 'line'  }
                 />
            </div>
        );
    }
}

export default Input;
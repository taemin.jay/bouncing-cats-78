import React, {Component} from 'react'
import Form from './Form'
import {connect} from 'react-redux';

import './registration.css'

class Registration extends Component {

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    render() {
        return (
            <div>
                <section className={this.props.feedback.feedbacks.find(x => x.page === "registration").show ? `registration-form registration-form-show` : 'registration-form'}>
                    <div className="container d-flex flex-row justify-content-between">
                        <div className="block d-flex flex-row align-items-start justify-content-between">
                            <div className="position-absolute registration-block flexable">
                                <Form page="registration"/>
                            </div>
                        </div>
                        <button onClick={this.showRegBlock} className="registration-close-btn" type="button"><img alt='any text' src={`${this.props.schoolinfo.url}close-btn.svg`}/></button>
                    </div>
                </section>
            </div>
        )
    }
}

export default connect(
    state => ({
        feedback: state.feedback,
        schoolinfo: state.schoolinfo
    }),
    dispatch => ({
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean, additional: null})
        }
    }))(Registration);





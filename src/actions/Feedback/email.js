const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: '587',
    secure: false,
    auth: {
        user: 'bouncingcats78@gmail.com',
        pass: 'Qwerty12#'
    }
});

var mailOptions = {
    from: 'bouncingcats78@gmail.com',
    to: 'bouncingcats78@gmail.com',
    subject: 'TEST Sending Email using Node.js',
    text: 'That was easy!'
};

let sendEmail = () => {
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    })
};

module.exports = {
    sendEmail
}
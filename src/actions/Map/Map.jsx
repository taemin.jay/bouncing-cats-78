import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';

const { compose } = require("recompose");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");

class Map extends Component {
    render() {
        const GoogleMapExample = compose(
            withScriptjs,
            withGoogleMap
        )(props => (
            <GoogleMap
                defaultZoom={16}
                defaultCenter={{ lat:59.962791, lng: 30.294329 }}
            >
                <MarkerWithLabel
                    position={{ lat: 59.962791, lng: 30.294329 }}
                    labelAnchor={new window.google.maps.Point(-20, 17)}
                    icon={{url: `http://test.bouncingcats78.ru/bc-icon.png`, anchor: {x: 25, y: 20}, scaledSize: {width: 40, height: 40} }}
                    labelStyle={{color: "red", width: "100px"}}
                >
                    <div><span style={{fontSize: "12px", fontWeight: "800" , fontFamily:"Open Sans"}}>Чкаловский проспект, 15</span></div>
                </MarkerWithLabel>
            </GoogleMap>
        ));
        return(
            <div>
                <GoogleMapExample
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXDbCOcU8srQwQH6AV6XAHdcoisRCmJog&v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{position: "absolute", height: "100%", width: "100%" }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                />
            </div>
        );
    }
};
export default Map;

import React, { Component } from 'react';
import Slide from './slide';
import LeftArrow from './left-arrow';
import RightArrow from './right-arrow';

import './sliders.css'

class Sliders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: this.props.list,
            previousIndex: this.props.list.length - 1,
            currentIndex: 0,
            nextIndex: 1,
            decor: this.props.decor,
            hidden: false,
            stopShow: false,
            stopHide: false,
            stopClick: false,
            moveDirection: "left"
        }
    }

    componentDidMount() {

        if (this.props.timer) {

            this[this.props.timer.name] = setInterval(
                () => this.showNextSlide(),
                this.props.timer.delay
            );

        }
    }

    componentWillUnmount() {

        clearInterval(this[this.props.timer.name]);

    }

    goToPrevSlide = () => {

        if (!this.state.stopClick) {

            this.setState({
                stopShow: true,
                stopHide: true,
                stopClick: true,
                moveDirection: "right"
            })

            let state

            this.componentWillUnmount()

            if (this.state.currentIndex === 0) {

                state =
                    {
                        currentIndex: this.state.list.length - 1,
                        nextIndex: 0,
                        previousIndex: this.state.list.length - 2
                    }

            } else if (this.state.previousIndex === 0) {

                state =
                    {
                        currentIndex: 0,
                        nextIndex: 1,
                        previousIndex: this.state.list.length - 1
                    }

            } else if (this.state.nextIndex === 0) {

                state =
                    {
                        currentIndex: this.state.list.length - 2,
                        nextIndex: this.state.list.length - 1,
                        previousIndex: this.state.list.length - 3
                    }

            } else {

                state =
                    {
                        currentIndex: (this.state.currentIndex - 1) % this.state.list.length,
                        nextIndex: (this.state.nextIndex - 1) % this.state.list.length,
                        previousIndex: (this.state.previousIndex - 1) % this.state.list.length
                    }

            }

            this.showNextSlide(state)

            this.componentDidMount()

        }
    }


    goToNextSlide = () => {

        if (!this.state.stopClick) {

            this.setState({
                stopShow: true,
                stopHide: true,
                stopClick: true,
                moveDirection: "left"
            })

            this.componentWillUnmount()

            this.showNextSlide()

            this.componentDidMount()

        }
    }

    showInfoByClickTimeout = (delay) => {

        setTimeout(() => {

            this.changeHiddenInfo()

            this.setState({
                stopClick: false,
                moveDirection: "left"
            })
        },delay)
    }

    hideInfoByClickTimeout = (delay) => {

        setTimeout(() => {

            this.changeHiddenInfo()

        },delay)
    }

    showNextSlide = (state) => {

        if (!this.state.stopClick) {
            this.hideInfoByClickTimeout(0)
        } else {
            this.hideInfoTimeout(0)
        }

        if (state) {

            setTimeout(() => {
                this.setState(state)
            }, 1000)

        } else {

            setTimeout(() => {
                this.setState({
                    currentIndex: (this.state.currentIndex + 1) % this.state.list.length,
                    nextIndex: (this.state.nextIndex + 1) % this.state.list.length,
                    previousIndex: (this.state.previousIndex + 1) % this.state.list.length
                })
            }, 1000)

        }

        if (!this.state.stopClick) {
            this.showInfoByClickTimeout(2000)
        } else {
            this.showInfoTimeout(2000)
        }

    }

    showInfoTimeout = (delay) => {

        setTimeout(() => {
            if (this.state.stopShow === true) {

                this.disableStopShow()

                return

            }

            this.changeHiddenInfo()

        },delay)
    }

    disableStopShow () {

        this.setState(
            {
                stopShow: false
            }
        )
    }

    hideInfoTimeout = (delay) => {

        setTimeout(() => {
            if (this.state.stopHide === true) {

                this.disableStopHide()

                return


            }

            this.changeHiddenInfo()

        },delay)
    }

    disableStopHide() {

        this.setState(
            {
                stopHide: false
            }
        )
    }

    changeHiddenInfo() {

        this.setState(
            {
                hidden: !this.state.hidden
            }
        )

    }

    render() {

        return (
            <div className={["slider " + this.props.clNameSlide]}>
                <div className="slider-wrapper">
                    <Slide key={this.state.previousIndex} list={this.state.list[this.state.previousIndex]} decor={this.state.decor} hidden={this.state.hidden} clName={`slider-previous`} moveDirection={this.state.moveDirection}/>
                    <Slide key={this.state.currentIndex} list={this.state.list[this.state.currentIndex]} decor={this.state.decor} hidden={this.state.hidden} clName={`slider-current`} moveDirection={this.state.moveDirection}/>
                    <Slide key={this.state.nextIndex} list={this.state.list[this.state.nextIndex]} decor={this.state.decor} hidden={this.state.hidden} clName={`slider-next`} moveDirection={this.state.moveDirection}/>

                    <LeftArrow goToPrevSlide={this.goToPrevSlide} />
                    <RightArrow goToNextSlide={this.goToNextSlide} />
                </div>


            </div>
        );
    }
}

export default Sliders
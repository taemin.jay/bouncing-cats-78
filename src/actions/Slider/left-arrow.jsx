import React from 'react';

const LeftArrow = (props) => {
    return (
        <div className="backArrow" onClick={props.goToPrevSlide}>
            <img className="arrow" src={`https://bouncingcats78.ru/arrowLeft.svg`} alt="" />
        </div>
    );
}

export default LeftArrow;

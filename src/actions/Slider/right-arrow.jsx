import React from 'react';

const RightArrow = (props) => {
    return (
        <div className="nextArrow" onClick={props.goToNextSlide}>
            <img className="arrow" src={`https://bouncingcats78.ru/arrowRight.svg`} alt="" />
        </div>
    );
}


export default RightArrow;
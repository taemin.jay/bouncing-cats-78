import React, { Component } from 'react';
import {connect} from 'react-redux';

class Slide extends Component {

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    showSlide = (list,decor,clName, hidden, movedirection) => {
        let styles, directionClassOut, directionClassIn

        if (movedirection === "left") {
            directionClassOut = "move-out-text-to-left"
            directionClassIn = "move-in-text-to-left"
        } else {
            directionClassOut = "move-out-text-to-right"
            directionClassIn = "move-in-text-to-right"
        }

        switch (decor) {
            case "deckor-contain-stocks":

                let class1 = 'slide-info slide-info-left d-flex flex-column justify-content-center',
                    class2 = 'slide-info slide-info-right d-flex flex-column justify-content-center'

                if (window.innerWidth > 520) {
                    styles = {
                        backgroundImage: `url(${list.img})`,
                        backgroundSize: 'contain',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center'
                    }

                    return (
                        <div className={`slide d-flex flex-row justify-content-between ${clName}`} style={styles}>

                            <div className={hidden ? `${directionClassOut} ${class1}` : `${directionClassIn} ${class1}`}>
                                <h3>{list.name}</h3>
                                <h4>{list.event}</h4>
                            </div>

                            <div className={hidden ? `${directionClassOut} ${class2}` : `${directionClassIn} ${class2}`}>

                                <p>{list.desc}</p>
                                <div className="registration">
                                    <button className="draw in-block" onClick={this.showRegBlock}
                                            type="button">Записаться
                                    </button>
                                </div>

                            </div>
                        </div>
                    )
                } else {
                    styles = {
                        backgroundImage: `url(${list.img})`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        height: '200px',
                        position: 'relative'
                    }

                    return (
                        <div className={`slide ${clName}`}>
                            <div className={hidden ? `${directionClassOut} ${class1}` : `${directionClassIn} ${class1}`}>

                                <h3>{list.name}</h3>
                                <h4>{list.event}</h4>

                            </div>
                            <div style={styles}>
                            </div>
                            <div className={hidden ? `${directionClassOut} ${class2}` : `${directionClassIn} ${class2}`}>

                                <p>{list.desc}</p>
                                <div className="registration">
                                    <button className="draw in-block" onClick={this.showRegBlock}
                                            type="button">Записаться
                                    </button>
                                </div>

                            </div>
                        </div>
                    )
                }

            case "deckor-contain-gallery":
                styles = {
                    backgroundImage: `url(${list.img})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: '50% 35%'
                }

                return (
                    <div className={`slide d-flex flex-row justify-content-between ${clName}`} style={styles}>
                    </div>
                )
            case "deckor-contain-achievment":

                if (window.innerWidth > 768) {

                    styles = {
                        backgroundImage: `url(${list.img})`,
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat'
                    }
                    let class1 = "slide-info slide-info-left d-flex flex-column justify-content-center"
                    return (
                        <div className={`slide ${clName}`} style={styles}>
                            <div className={hidden ? `${directionClassOut} ${class1}` : `${directionClassIn} ${class1}`}>

                                <h3>{list.name}</h3>
                                <h5>{list.regards}</h5>
                                <p>{list.desc}</p>

                            </div>
                        </div>
                    )

                } else {

                    styles = {
                        backgroundImage: `url(${list.img})`,
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        height: '300px',
                        position: 'relative'
                    }
                    let class3 = "slide-info slide-info-left d-flex flex-column justify-content-center"

                    return (
                        <div className={`slide ${clName}`}>
                            <div style={styles}>
                            </div>
                            <div className={hidden ? `${directionClassOut} ${class3}` : `${directionClassIn} ${class3}`}>

                                <h3>{list.name}</h3>
                                <h5>{list.regards}</h5>
                                <p>{list.desc}</p>

                            </div>
                        </div>
                    )

                }
            default:
                return null
        }
    }

    render () {
        return this.showSlide(this.props.list, this.props.decor, this.props.clName, this.props.hidden, this.props.moveDirection)
    }
}

export default connect(
    state => ({
        feedback: state.feedback
    }),
    dispatch => ({
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        }
    }))(Slide)
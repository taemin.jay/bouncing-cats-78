import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import {connect} from "react-redux";

class Footer extends Component {

    showCurrentYear () {
        return new Date().getFullYear()
    }

    showList () {
        return  this.props.schoolinfo.social
            .filter( social => social.disabled )
            .map( social => {
                return (
                        <a target='_blank' rel="noopener noreferrer" key={social.id} href={social.url} title={social.name}><img className="" src={`${this.props.schoolinfo.url}${social.icon}`} alt="" /></a>
                )

            });
    }

    render() {
        return (
            <footer>
                <div className="container d-flex justify-content-center">
                    <ul className="block d-flex flex-row align-items-center justify-content-between">
                        <li id="copyright" className="copyright">
                            ©Bouncing Cats, 2017-{this.showCurrentYear()}
                        </li>
                        <li id="telephone" className="telephone">
                            <Link to='tel:+78129081525'>908-15-25</Link>
                        </li>
                        <li id="social" className="social">
                            {this.showList()}
                        </li>
                        <li id="address" className="address">
                            <a target="_blank"
                               href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1997.0918802139515!2d30.29359001598234!3d59.96379936703082!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4696314ed99f7d5d%3A0xdbfd85e0ec8eab0a!2z0KfQutCw0LvQvtCy0YHQutC40Lkg0L_RgC4sIDE1LCDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywgMTk3MTEw!5e0!3m2!1sru!2sru!4v1538039948390">СПб,
                                Чкаловский пp. 15</a>
                        </li>
                        <li className="designer">Дизайнер: <b>Трещев</b></li>
                    </ul>
                </div>
            </footer>
        )
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(Footer);

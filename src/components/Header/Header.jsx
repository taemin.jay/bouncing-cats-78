import { Link } from 'react-router-dom'
import RouteList from '../../containers/router-list'
import React, { Component } from 'react';
import {connect} from 'react-redux';

class Header extends Component {

    showHeader(show) {

        if (window.innerWidth>520) {
            return (
                <div className="block d-flex flex-row align-items-center justify-content-between">
                    <div className="logo">
                        <nav>
                            <Link to='/'><img alt='any text' src={`${this.props.schoolinfo.url}logo.svg`}/></Link>
                        </nav>
                    </div>
                    <RouteList/>
                    <div className="registration">
                        <button onClick={this.showRegBlock} type="button">Записаться</button>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="block d-flex flex-row align-items-center justify-content-between">
                    <div className="logo">
                        <nav>
                            <Link to='/'><img alt='any text' src={`${this.props.schoolinfo.url}logo.svg`}/></Link>
                        </nav>
                    </div>

                    <div className="menu-burger">
                        <div onClick={this.showMenu} className="menu-burger-open"><img alt='any text'
                                                               src={`${this.props.schoolinfo.url}menu-burger.svg`}/>
                        </div>
                    </div>
                    <RouteList clName="menu-mobile" show={show}/>
                </div>
            )
        }

    }

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    showMenu = () => {
        this.props.showMenu(!this.props.router.show)
    }

    render() {
        return (
            <header>
                <div className="container d-flex justify-content-center">
                    {this.showHeader(this.props.router.show)}
                </div>
            </header>
        )
    }
}

export default connect(
    state => ({
        feedback: state.feedback,
        router: state.router,
        schoolinfo: state.schoolinfo
    }),
    dispatch => ({
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        },
        showMenu: (boolean) => {
            dispatch({type: 'SHOW_MENU', payload: boolean})
        }
    }))(Header);
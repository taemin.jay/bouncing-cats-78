import React from 'react'
import HeadersBottomList from '../../containers/header-bottom-list'
import {Provider} from 'react-redux'
import allReducers from "../../reducers";
import {createStore} from 'redux'

const store = createStore (allReducers);

const HeaderBottom = () => (
    <div className="header-bottom">
        <Provider store={store}>
            <HeadersBottomList />
        </Provider>
    </div>
)

export default HeaderBottom
import React, {Component} from 'react'
import HeadersTitleList from '../../containers/header-title-list'

class HeaderTitle extends Component {
    render() {
        return <div className="header-title">
            <HeadersTitleList page={this.props.page}/>
        </div>
    }
}

export default HeaderTitle
import React from 'react'

const Children = () => (
    <div>
        <h1>Welcome to Children!</h1>
    </div>
)

export default Children
import React from 'react'
import ContactHeader from "./ContactHeader"
import ContactFeedback from "./ContactFeedback"
import ContactMap from "./ContactMap";

import '../../../assets/css/contact.css'


const Contact = () => (
    <section className="contact">
        <ContactHeader />
        <ContactFeedback />
        <ContactMap />
    </section>
)

export default Contact
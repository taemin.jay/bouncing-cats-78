import React from 'react'
import Form from '../../../actions/Feedback/Form'

const PriceContacts = () => (
    <section className="contact-feedback pt-100">
        <div className="container d-flex flex-row justify-content-between">
            <div className="block d-flex flex-row align-items-start justify-content-between">
                <Form page="contact" />
            </div>
        </div>
    </section>
)

export default PriceContacts;



import React from 'react'
import ContactHeaderList from '../../../containers/contact-header-list'

const ScheduleHeader = () => (
    <section className="contact-header pt-100">
        <div className="container d-flex justify-content-center">
            <ContactHeaderList />
        </div>
    </section>
)

export default ScheduleHeader;
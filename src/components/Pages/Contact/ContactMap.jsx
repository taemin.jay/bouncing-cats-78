import React from 'react'
import MapContainer from '../../../actions/Map/Map'

const ContactMap = () => (
    <section className="contact-map mt-100">
        <div className="position-relative map">
            <MapContainer />
        </div>
    </section>
)

export default ContactMap;
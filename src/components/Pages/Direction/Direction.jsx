import React from 'react'
import DirectionHeader from './DirectionHeader'
import DirectionContant from './DirectionContant'

import HeaderBottom from "../../Header/HeaderBottom";
import HeaderTitle from "../../Header/HeaderTitle"

import '../../../assets/css/direction.css'


const Direction = () => (
    <section>
        <div className="header-all" style={{backgroundImage: 'url("https://bouncingcats78.ru/fone_direction.jpg")',  backgroundPosition:'90%'}}>
            <HeaderTitle page="direction" />
            <HeaderBottom/>
            <DirectionHeader />
        </div>
        <DirectionContant />
    </section>
)

export default Direction
import React from 'react'
import {createStore} from 'redux'
import allReducers from "../../../reducers"
import DirectionList from '../../../containers/direction-list'
import {Provider} from 'react-redux'

const store = createStore (allReducers);

const DirectionContant = () => (
    <section id="anchor" className="direction-contant pt-100">
        <div className="container d-flex justify-content-center">
            <Provider store={store}>
                <DirectionList />
            </Provider>
        </div>
    </section>
)

export default DirectionContant
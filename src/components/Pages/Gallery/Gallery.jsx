import React from 'react'
import GalleryList from '../../../containers/gallery-list'

import '../../../assets/css/gallery.css'

const Gallery = () => (
    <section className="gallery">
        <GalleryList />
    </section>
)

export default Gallery
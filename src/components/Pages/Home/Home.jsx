import React, {Component} from 'react'
import HomeHeader from './HomeHeader'
import HomeDirections from './HomeDirections'
import HomeDirectionsInfo from './HomeDirectionsInfo'
import HomeSchoolInfo from './HomeSchoolInfo'
import HomeAchievements from './HomeAchievements'
import HomeBenefits from './HomeBenefits'
import HomeAbonements from './HomeAbonements'
import HomeStocks from './HomeStocks'
import HomeGallery from './HomeGallery'

import HeaderBottom from "../../Header/HeaderBottom";
import HeaderTitle from "../../Header/HeaderTitle"

import '../../../assets/css/home.css'

let style
if (window.innerWidth < 520) {
        style = {
            backgroundImage: 'url("https://bouncingcats78.ru/fone_main.jpg")'
        }
} else {
        style = {
        backgroundColor: 'black'
        }
}

const Home = () => (
    <section>
        <div className="header-all" style={style}>
                <HeaderTitle page="schoolinfo"/>
                <HeaderBottom/>
                <HomeHeader/>
        </div>
        <HomeDirections/>
        <HomeDirectionsInfo/>
        <HomeSchoolInfo/>
        <HomeAchievements/>
        <HomeBenefits/>
        <HomeAbonements/>
        <HomeStocks/>
        <HomeGallery/>
    </section>
)


export default Home
import React from 'react'
import AbonementsList from '../../../containers/abonement-list'

const HomeAbonements = () => (
    <section className="home-abonement pt-100">
        <div className="container d-flex justify-content-center">
             <AbonementsList page="home" />
        </div>
    </section>
)

export default HomeAbonements
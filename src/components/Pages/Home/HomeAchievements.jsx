import React from 'react'
import AchievementsList from '../../../containers/achievements-list'

const HomeAchievements = () => (
    <section className="home-achievements pt-100 pb-100">
        <div className="container d-flex justify-content-center">
            <AchievementsList />
        </div>
    </section>
)

export default HomeAchievements
import React from 'react'
import BenefitsList from '../../../containers/benefits-list'

const HomeBenefits = () => (
    <section className="home-benefits pt-100">
        <div className="container d-flex justify-content-center">
            <BenefitsList />
        </div>
    </section>
)

export default HomeBenefits
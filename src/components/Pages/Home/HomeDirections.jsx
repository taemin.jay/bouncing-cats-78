import React from 'react'
import DirectionList from '../../../containers/home-direction-list'


const HomeDirections = () => (
    <section id="anchor" className="home-direction pt-100">
        <div className="container d-flex justify-content-center">
            <div className="block d-flex flex-column justify-content-between">
                <h2>Направления</h2>
                <DirectionList />
           </div>
        </div>
    </section>
)

export default HomeDirections
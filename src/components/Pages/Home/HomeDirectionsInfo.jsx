import React, {Component} from 'react'
import {connect} from 'react-redux';

class HomeDirectionsInfo extends Component {

    showDirInfo = (index) => {
        this.props.showDirInfo(!this.props.direction.directions.find(x => x.id === index).show, index)
    }

    showP (info) {
        return info.map( (info, index) => {
            return (<p key={index}>{info}</p>)
        })

    }

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    closeDirInfo = () => {
        this.props.closeDirInfo()
    }

    showInfo = () => {
        const info = this.props.direction.directions
            .filter(x => x.disabled)
            .filter(x => x.show)[0]
        if (info) {
            let img = `${this.props.schoolinfo.url}${info.poster}`,
                poster = `${this.props.schoolinfo.url}opacity.png`,
                style = {
                    backgroundImage: `url(${img})`,
                    backgroundSize: "100% 100%",
                    backgroundRepeat: "no-repeat"
                }
            return (
                <section className="direction-form">
                    <div onClick={this.closeDirInfo}  className="direction-form-back"></div>
                    <div className="container d-flex flex-row justify-content-between">
                        <div className="block d-flex flex-row align-items-start justify-content-between">
                            <div className="position-absolute direction-info-block d-flex flex-column">
                                <video controls style={style} poster={poster} className="video">
                                    <source src={`${this.props.schoolinfo.url}${info.video}`} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                                </video>
                                <div className="d-flex flex-column direction-info-text">
                                    <h3>{info.name}</h3>
                                    <div className="direction-info-text-block">
                                        {this.showP(info.info)}
                                    </div>
                                    <button className="registration" onClick={this.showRegBlock} type="button">Записаться на занятие</button>
                                </div>
                                <button key={info.id} onClick={() => this.showDirInfo(info.id)} className="registration-close-btn" type="button"><img alt='any text' src={`${this.props.schoolinfo.url}close-btn.svg`}/></button>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return null
        }
    }

    render() {
        return (this.showInfo())
    }
}

export default connect(
    state => ({
        direction: state.direction,
        feedback: state.feedback,
        schoolinfo: state.schoolinfo
    }),
    dispatch => ({
        showDirInfo: (boolean, index) => {
            dispatch({type: 'SHOW_DIRBLOCK', payload: boolean, id: index})
        },
        closeDirInfo: () => {
            dispatch({type: 'CLOSE_INFOBLOCK'})
        },
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        }
    }))(HomeDirectionsInfo);





import React from 'react'
import GalleryList from '../../../containers/gallery-list'

const HomeGallery = () => (
    <section className="home-gallery">
        <GalleryList />
    </section>
)

export default HomeGallery
import React from 'react'

const HomeHeader = () => {
    if (window.innerWidth >= 520) {
        return (
            <div className="header" style={{background: 'black'}}>
                <div className="black"></div>
                <video autoPlay loop muted rel="preload" className="video">
                    <source src={`https://bouncingcats78.ru/fone.mp4`} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                </video>
            </div>
        )
    }
    return (
        <div className="header">
            <div className="black"></div>
        </div>
    )
}

export default HomeHeader
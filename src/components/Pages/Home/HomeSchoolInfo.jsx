import React from 'react'
import ShoolInfoList from '../../../containers/schoolinfo-list'

const HomeSchoolInfo = () => (
    <section className="home-schoolinfo pt-100">
        <div className="container d-flex justify-content-center">
             <ShoolInfoList />
        </div>
    </section>
)

export default HomeSchoolInfo
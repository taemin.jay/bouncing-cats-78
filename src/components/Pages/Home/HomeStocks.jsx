import React from 'react'
import StocksList from '../../../containers/stock-list'

const HomeStocks = () => (
    <section className="home-stocks">
        <div className="container d-flex justify-content-center">
            <StocksList />
        </div>
    </section>
)

export default HomeStocks
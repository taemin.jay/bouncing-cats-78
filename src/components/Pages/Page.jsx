import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom'
import Home from './Home/Home'
import Children from './Children/Children'
import Teacher from './Teacher/Teacher'
import Schedule from "./Schedule/Schedule";
import Direction from "./Direction/Direction";
import Price from './Price/Price';
import Contact from "./Contact/Contact";
import Gallery from './Gallery/Gallery';


class ScrollToTop extends Component {
    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            window.scrollTo(0, 0)
        }
    }

    render() {
        return this.props.children
    }
}

const Page = () => (
    <Switch>
        <ScrollToTop>
            <Route exact path='/' component={Home}/>
            <Route path='/children' component={Children}/>
            <Route path='/teacher' component={Teacher}/>
            <Route path='/schedule' component={Schedule}/>
            <Route path='/direction' component={Direction}/>
            <Route path='/price' component={Price}/>
            <Route path='/contact' component={Contact}/>
            <Route path='/gallery' component={Gallery}/>
        </ScrollToTop>
    </Switch>
);

export default Page
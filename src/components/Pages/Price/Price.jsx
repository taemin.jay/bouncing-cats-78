import React from 'react'
import PriceHeader from './PriceHeader'
import PriceAbonements from './PriceAbonements'
import PriceContacts from './PriceContacts'
import PriceRules from './PriceRules'
import PriceStocks from './PriceStocks'

import HeaderBottom from "../../Header/HeaderBottom";
import HeaderTitle from "../../Header/HeaderTitle"

import '../../../assets/css/abonement.css'


const Price = () => (
    <section>
        <div className="header-all" style={{backgroundImage: 'url("https://bouncingcats78.ru/fone_abonement.jpg")', backgroundPosition:'15%'}}>
                <HeaderTitle page="abonement" />
                <HeaderBottom/>
                <PriceHeader />
        </div>
        <PriceAbonements />
        <PriceContacts />
        <PriceStocks />
        <PriceRules />
    </section>
)

export default Price
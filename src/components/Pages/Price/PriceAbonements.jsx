import React from 'react'
import AbonementsList from '../../../containers/abonement-list'

const PriceAbonements = () => (
    <section id="anchor" className="price-abonement pt-100">
        <div className="container d-flex justify-content-center">
            <AbonementsList page="abonement" />
        </div>
    </section>
)

export default PriceAbonements
import React from 'react'
import Form from '../../../actions/Feedback/Form'

import MapContainer from '../../../actions/Map/Map'

const PriceContacts = () => {

    if (window.innerWidth > 520) {
        return (
            <section className="price-contact mt-100">
                <div className="container d-flex flex-row justify-content-between">
                    <div className="block d-flex flex-row align-items-start justify-content-between">
                        <div className="position-relative price-feedback flexable">
                            <Form page="abonement"/>
                        </div>
                        <div className="position-relative flexable h-100">
                            <MapContainer/>
                        </div>
                    </div>
                </div>
            </section>
        )
    } else {
        return (
            <section className="price-contact mt-100">
                <div className="container d-flex flex-row justify-content-between">
                    <div className="block d-flex flex-column align-items-start justify-content-between">
                        <div className="position-relative price-feedback flexable mb-100">
                            <Form page="abonement"/>
                        </div>
                        <div className="position-relative price-map flexable h-100 mb-100">
                            <MapContainer/>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default PriceContacts;



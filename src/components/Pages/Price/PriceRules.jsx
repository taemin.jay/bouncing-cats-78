import React from 'react'
import RulesList from '../../../containers/rules-list'

const PriceRules = () => (
    <section className="price-rules mt-100">
        <div className="container d-flex justify-content-center">
            <RulesList />
        </div>
    </section>
)

export default PriceRules
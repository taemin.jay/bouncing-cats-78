import React from 'react'
import StocksList from '../../../containers/stock-list'

const PriceStocks = () => (
    <section className="price-stocks mt-100">
        <div className="container d-flex justify-content-center">
            <StocksList page="abonement" />
        </div>
    </section>
)

export default PriceStocks
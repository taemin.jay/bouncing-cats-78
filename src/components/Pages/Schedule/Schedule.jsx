import React from 'react'
import ScheduleHeader from "./ScheduleHeader"
import ScheduleTable from "./ScheduleTable"

import '../../../assets/css/schedule.css'

const Schedule = () => (
    <section className="schedule">
        <ScheduleHeader />
        <ScheduleTable />
    </section>
)

export default Schedule
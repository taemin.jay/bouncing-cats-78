import React from 'react'
import SheduleHeaderList from '../../../containers/schedule-header-list'

const ScheduleHeader = () => (
    <section className="schedule-header pt-100">
        <div className="container d-flex justify-content-center">
            <SheduleHeaderList />
        </div>
    </section>
)

export default ScheduleHeader;
import React from 'react'
import SheduleTableList from '../../../containers/schedule-table-list'

const SheduleTable = () => (
    <section className="schedule-table-list pt-100 mb-100">
        <div className="container d-flex justify-content-center">
            <SheduleTableList />
        </div>
    </section>
)

export default SheduleTable
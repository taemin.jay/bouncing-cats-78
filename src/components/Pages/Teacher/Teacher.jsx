import React from 'react'
import HeaderBottom from "../../Header/HeaderBottom";
import HeaderTitle from "../../Header/HeaderTitle"
import TeacherHeader from "./TeacherHeader"
import TeacherList from "./TeacherList"
import TeacherListInfo from "./TeacherListInfo"



import '../../../assets/css/teacher.css'

const Teacher = () => (
    <section>
        <div className="header-all" style={{backgroundImage: 'url("https://bouncingcats78.ru/fone_teacher.jpg")', backgroundPosition:'55%'}}>
            <HeaderTitle page="teachers" />
            <HeaderBottom/>
            <TeacherHeader />
        </div>
        <TeacherList />
        <TeacherListInfo />
    </section>
)

export default Teacher
import React from 'react'
import TeachersList from '../../../containers/teacher-list'


const TeacherList = () => (
    <section id="anchor" className="teacher-list mb-100">
        <div className="container d-flex justify-content-center">
            <TeachersList />
        </div>
    </section>
)

export default TeacherList
import React, {Component} from 'react'
import {connect} from 'react-redux';

class TeacherListInfo extends Component {

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    showDirInfo = (index) => {
        this.props.showDirInfo(!this.props.teachers.list.find(x => x.id === index).show, index)
    }

    showP (info) {
        return info.map( (info, index) => {
            return (<p key={index}>{info}</p>)
        })

    }

    showMedia = (info) => {
        let media1 = info.media.images.map( (image) => {
            const img = `${this.props.schoolinfo.url}${image}`
            var styles = {
                backgroundImage: `url(${img})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: '60% 50%'
            }
            return (<div key={image} className='teacher-media-img' style={styles}> </div>)
        })

        const media2 =  info.media.video.map( (video) => {
            let img = `${this.props.schoolinfo.url}${video.poster}`,
                poster = `${this.props.schoolinfo.url}opacity.png`,
                style = {
                    backgroundImage: `url(${img})`,
                    backgroundSize: "100% 100%",
                    backgroundRepeat: "no-repeat"
                }
            return (<video key={video.video} controls className="video" style={style} poster={poster}>
                <source src={`${this.props.schoolinfo.url}${video.video}`} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
            </video>)
        })
        return media1.concat(media2)
    }

    convertTime (houre) {
        return houre+".00"
    }

    showSchedule = (info) => {
        const days = Object.keys(this.props.schedule.schedules)
        let array = [], output = [], index = 0
        days.forEach(day => {
            let Day = this.props.schedule.dayofweek.find(x => {
                    return x.includes(day)
                })[0],
                schedule = this.props.schedule.schedules[day]
                    .filter(x => x.teacher === info.name)
                    .filter(x => {
                        let y = this.props.schedule.type[0],
                            v = y.list.find(b => b.name === x.name)
                        return v.disabled
                    })
            array.push({day: Day, schedule: schedule})
        })

        array = array.filter( x => x.schedule.length !== 0)

        array.forEach( (array) => {
            for (let i=0; i<array.schedule.length; i++) {
                index += i
                output.push(
                    <div key={index} className="d-flex flex-row justify-content-between">
                        <h5>{array.day}</h5>
                        <h5>{array.schedule[i].name}</h5>
                        <h5>{this.convertTime(array.schedule[i].time)}</h5>
                    </div>
                )
            }
            index++
        })

        return output

    }

    showInfo = () => {
        const info = this.props.teachers.list
            .filter(x => x.disabled)
            .filter(x => x.show)[0]

        if (info) {
            if (window.innerWidth > 520) {
                return (
                    <section className="teachers-form pt-100">
                        <div className="container d-flex flex-row justify-content-between">
                            <div className="block d-flex flex-row align-items-start justify-content-between">
                                <div className="teachers-info-block d-flex flex-column">
                                    <div className="d-flex flex-column teachers-info-text teacher-list">
                                        <div className="d-flex flex-row flex-wrap justify-content-between mb-100">
                                            <div className="d-flex flex-column">
                                                <h3>{info.name}</h3>
                                                <h4>{info.styles.join(", ")}</h4>
                                            </div>
                                            <h5>{info.position.join(", ")}</h5>
                                        </div>
                                        <div
                                            className="d-flex flex-row flex-wrap align-items-center justify-content-between mb-100">
                                            <div className="img-teacher-list"><img
                                                src={`${this.props.schoolinfo.url}${info.badge}`} alt=""/></div>
                                            <div>{this.showP(info.info)}</div>
                                        </div>
                                        <div className="d-flex flex-column teachers-schedule mb-100">
                                            <h4>Расписание</h4>
                                            {this.showSchedule(info)}
                                            <div className="registration">
                                                <button type="button" onClick={this.showRegBlock}>Записаться</button>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-row flex-wrap mb-100">
                                            {this.showMedia(info)}
                                        </div>
                                        <button key={info.id} onClick={() => this.showDirInfo(info.id)}
                                                className="registration-close-btn" type="button"><img alt='any text'
                                                                                                      src={`${this.props.schoolinfo.url}close-btn-teach.svg`}/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                )
            } else {
                return (
                    <section className="teachers-form pt-100 pb-100">
                        <div className="container d-flex flex-row justify-content-between">
                            <div className="block d-flex flex-row align-items-start justify-content-between">
                                <div className="teachers-info-block d-flex flex-column">
                                    <div className="d-flex flex-column teachers-info-text teacher-list">
                                        <div className="d-flex flex-row flex-wrap justify-content-between mb-100">
                                            <div className="d-flex flex-column title-name">
                                                <h3>{info.name}</h3>
                                                <h4>{info.styles.join(", ")}</h4>
                                            </div>
                                            <div className="img-teacher-list"><img
                                                src={`${this.props.schoolinfo.url}${info.badge}`} alt=""/></div>
                                        </div>
                                        <div
                                            className="d-flex flex-row flex-wrap align-items-center justify-content-between mb-100">
                                            <h5>{info.position.join(", ")}</h5>
                                            <div>{this.showP(info.info)}</div>
                                        </div>
                                        <div className="d-flex flex-column teachers-schedule mb-100">
                                            <h4>Расписание</h4>
                                            {this.showSchedule(info)}
                                            <div className="registration">
                                                <button type="button" onClick={this.showRegBlock}>Записаться</button>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-row flex-wrap mb-100">
                                            {this.showMedia(info)}
                                        </div>
                                        <button key={info.id} onClick={() => this.showDirInfo(info.id)}
                                                className="registration-close-btn" type="button"><img alt='any text'
                                                                                                      src={`${this.props.schoolinfo.url}close-btn-teach.svg`}/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                )
            }
        } else {
            return null
        }
    }

    render() {
        return (this.showInfo())
    }
}

export default connect(
    state => ({
        teachers: state.teachers,
        schedule: state.schedule,
        feedback: state.feedback,
        schoolinfo: state.schoolinfo
    }),
    dispatch => ({
        showDirInfo: (boolean, index) => {
            dispatch({type: 'SHOW_INFOBLOCK', payload: boolean, id: index})
        },
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        }
    }))(TeacherListInfo);





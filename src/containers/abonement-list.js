import React, {Component} from 'react';
import {connect} from 'react-redux';

class AbonementsList extends Component {
    showOptionList (array) {
        return array.map( (option, index) => {
            return (
                <p key={index}>{option}</p>
            )
        })
    }

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    checkBtnName (abonement) {

        if (abonement.btnName !== null) {
            return abonement.btnName
        }
        return this.props.abonements.btnName_default
    }

    showList (abonement) {
        return (
            <li key={abonement.id} className="mb-100">
                <div className="d-flex flex-row flex-wrap align-items-center justify-content-center block-abonement">
                    <h4>{abonement.name}</h4>
                    <h2>{abonement.price}<sup>{abonement.payment}</sup></h2>
                    <div className="abonement-options d-flex flex-column align-items-center">
                        {this.showOptionList(abonement.options)}
                    </div>
                    <h5>{abonement.reclame}</h5>
                    <div className="registration">
                        <button className="in-block" onClick={this.showRegBlock} type="button">{this.checkBtnName(abonement)}</button>
                    </div>
                    <div className="block-sale">
                        <p>{this.props.abonements.ad}</p>
                    </div>
                </div>
            </li>
        )
    }

    filterPage (page) {
        switch(page){
            case "abonement":
                return  this.props.abonements.list
                    .filter( abonement => abonement.disabled )
                    .map( abonement => {return this.showList(abonement)})
            case "home":
                return  this.props.abonements.list
                    .filter( abonement => abonement.disabled )
                    .filter( abonement => abonement.home_page)
                    .map( abonement => {return this.showList(abonement)})
            default:
                return null
        }
    }

    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2 dangerouslySetInnerHTML={{__html: this.props.abonements.name_home}} />
                <ul className="d-flex flex-row align-items-start justify-content-between flex-wrap mt-100">
                    {this.filterPage(this.props.page)}
                </ul>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        abonements: state.abonement,
        feedback: state.feedback
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        }
    }))(AbonementsList);
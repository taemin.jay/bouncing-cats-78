import React, {Component} from 'react';
import {connect} from 'react-redux';
import Sliders from '../actions/Slider/Sliders'

class AchievementsList extends Component {
    listPrepareAchiev (t) {
        return t
            .filter(achievements => achievements.disabled)
            .map(achievement => {return {
                    name: achievement.name,
                    regards: achievement.regards.join(", "),
                    desc: achievement.desc,
                    img: `${this.props.schoolinfo.url}${achievement.img}`
                }})
    }

    render () {

        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2 dangerouslySetInnerHTML={{__html: this.props.achievements.name}} />
                <Sliders timer={{name:"achieveTimer", delay: 10000}} list={this.listPrepareAchiev(this.props.achievements.achiev)} clNameSlide="slider-achievements mt-100 mb-100" decor="deckor-contain-achievment"/>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        achievements: state.achievements,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(AchievementsList);
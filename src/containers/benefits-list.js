import React, {Component} from 'react';
import {connect} from 'react-redux';

class BenefitsList extends Component {
    showList () {
        return  this.props.schoolinfo.more_benifit.list.filter( more_benifit => more_benifit.disabled ).map( more_benifit => {
            return (
                <li key={more_benifit.id} className="d-flex flex-column align-items-baseline justify-content-center">
                    <img className="img-benefit" src={`${this.props.schoolinfo.url}${more_benifit.img}`} alt=""/>
                    <h3>{more_benifit.name}</h3>
                    <p>{more_benifit.desc}</p>
                </li>
            )

        });
    }
    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2 dangerouslySetInnerHTML={{__html: this.props.schoolinfo.more_benifit.name}} />
                <ul className="d-flex flex-row align-items-baseline justify-content-between flex-wrap mt-100">
                    {this.showList()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(BenefitsList);
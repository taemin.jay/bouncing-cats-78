import React, {Component} from 'react';
import {connect} from 'react-redux';

class ContactHeaderList extends Component {

    showList () {
        return  this.props.schoolinfo.social.filter( social => social.disabled ).map( social => {
            return (
                <li key={social.id} className="d-flex flex-column align-items-baseline justify-content-center">
                    <a href={social.url} rel="noopener noreferrer" title={social.name} target='_blank'><img className="" src={`http://test.bouncingcats78.ru/${social.icon}`} alt="" /></a>
                </li>
            )

        });
    }

    showUl(contacts) {
        if (window.innerWidth > 520) {
            return (
                <ul className="d-flex flex-row align-items-center flex-wrap justify-content-between">
                    <li key="1">
                        <p>{contacts.address.name}</p>
                    </li>
                    <li key="2">
                        <h2>
                            <a href={"tel:" + contacts.telephone[1]}>{contacts.telephone[0]}</a>
                        </h2>
                    </li>
                    <li key="3">
                        <ul className="d-flex flex-row align-items-baseline justify-content-between flex-wrap">
                            {this.showList()}
                        </ul>
                    </li>
                </ul>
            )
        } else {
            return (
                <ul className="d-flex main flex-column align-items-baseline flex-wrap justify-content-between">
                    <li key="1">
                        <p>{contacts.address.name}</p>
                    </li>
                    <li key="2">
                        <h2>
                            <a href={"tel:" + contacts.telephone[1]}>{contacts.telephone[0]}</a>
                        </h2>
                    </li>
                    <li key="3">
                        <ul className="d-flex flex-row align-items-baseline justify-content-between flex-wrap">
                            {this.showList()}
                        </ul>
                    </li>
                </ul>
            )
        }
    }

    render () {
        const contacts = this.props.schoolinfo.contacts
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h3>{contacts.name}</h3>
                {this.showUl(contacts)}
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(ContactHeaderList);
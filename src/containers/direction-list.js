import React, {Component} from 'react';
import {connect} from 'react-redux';


class DirectionList extends Component {

    showP (info) {
        return info.map( (info, index) => {
            return (<p key={index}>{info}</p>)
        })

    }

    showList () {
        return this.props.direction.directions.filter( direction => direction.disabled).map( (direction,index) => {
            let img = `${this.props.schoolinfo.url}${direction.poster}`,
                poster = `${this.props.schoolinfo.url}opacity.png`
            let style = {
                backgroundImage: `url(${img})`,
                backgroundSize: "100% 100%",
                backgroundRepeat: "no-repeat"
            }
            if (index === 0 || index%2 === 0) {
                return (
                    <li key={direction.id} className="d-flex flex-row flex-wrap align-items-center justify-content-between mb-100">
                        <div className="img-rectangle d-flex align-items-center">
                            <div className="rectangle-left"><img src={`${this.props.schoolinfo.url}rectangle.svg`} alt=""/></div>
                            <video controls style={style} poster={poster} className="video">
                                <source src={`${this.props.schoolinfo.url}${direction.video}`} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                            </video>
                        </div>
                        <div className="benefit-schoolinfo">
                            <div className="d-flex flex-column benefit-right">
                                <h3>{direction.name}</h3>
                                {this.showP(direction.info)}
                            </div>
                        </div>
                    </li>
                )
            } else {
                return (
                    <li key={direction.id} className="d-flex flex-row-reverse flex-wrap align-items-center justify-content-between mb-100">
                        <div className="img-rectangle d-flex align-items-center">
                            <div className="rectangle-right"><img src={`${this.props.schoolinfo.url}rectangle.svg`} alt=""/></div>
                            <video controls poster={poster} style={style} className="video">
                                <source src={`${this.props.schoolinfo.url}${direction.video}`} type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
                            </video>
                        </div>
                        <div className="benefit-schoolinfo">
                            <div className="d-flex flex-column benefit-left">
                                <h3>{direction.name}</h3>
                                {this.showP(direction.info)}
                            </div>
                        </div>
                    </li>
                )
            }

        });
    }
    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <ul>
                    {this.showList()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        direction: state.direction,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(DirectionList);
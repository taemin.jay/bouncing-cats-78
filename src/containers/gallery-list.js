import React, {Component} from 'react';
import {connect} from 'react-redux';
import Sliders from '../actions/Slider/Sliders'

class GalleryList extends Component {
    listPrepareGalleryImage (t) {

        return t.images
            .map(image => {
                return {
                name: t.name,
                img: `${this.props.schoolinfo.url}gallery/${image}`
            }})
    }

    render () {
        return (
            <div>
                <div className="container">
                    <div className="block">
                        <div className="position-absolute mt-100" style={{zIndex: 15}}>
                            <h2>{this.props.gallery.name}</h2>
                        </div>
                    </div>
                </div>
                <Sliders timer={{name:"galleryTimer", delay: 5000}} list={this.listPrepareGalleryImage(this.props.gallery)} clNameSlide="slider-gallery mt-100" decor="deckor-contain-gallery"/>
            </div>
          );
    }
}

function mapStateToProps (state) {
    return {
        gallery: state.gallery,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(GalleryList);
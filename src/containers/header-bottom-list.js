import React, {Component} from 'react';
import {connect} from 'react-redux';
import { HashLink as Link } from 'react-router-hash-link';

class HeadersBottomList extends Component {
    showList () {
        return  this.props.schoolinfo.social
            .filter( social => social.mainScreenDisabled && social.disabled )
            .map( social => {
            return (
                <li key={social.id} className="d-flex flex-column align-items-baseline justify-content-center">
                    <a target='_blank' rel="noopener noreferrer" href={social.url} title={social.name}><img className="" src={`${this.props.schoolinfo.url}${social.iconMainScreen}`} alt="" /></a>
                </li>
            )

        });
    }

    getUrlPath () {
        return window.location.pathname
    }

    render () {
        return (
            <div className="container d-flex justify-content-center">
                <div className="block d-flex flex-row justify-content-between">
                    <ul className="d-flex flex-row align-items-baseline justify-content-between flex-wrap">
                        {this.showList()}
                    </ul>
                    <Link to={`${this.getUrlPath()}#anchor`} scroll={el => el.scrollIntoView({
                        behavior: 'smooth', block: 'start'
                    })}><img className="arrowDown" src={`${this.props.schoolinfo.url}arrowDown.svg`} alt='' /></Link>
                    <a href={"tel:" + this.props.schoolinfo.contacts.telephone[1]}>{this.props.schoolinfo.contacts.telephone[0]}</a>
                </div>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(HeadersBottomList);
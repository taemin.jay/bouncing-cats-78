import React, {Component} from 'react';

import {connect} from 'react-redux';

class HeadersTitleList extends Component {

    showRegBlock = () => {
        this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show)
    }

    showReclameBlock(reclame) {

        if (reclame) {

            let clName

            if (window.innerWidth > 768) {
                clName = "reclame slide-info-right d-flex flex-column justify-content-center"
            } else {
                clName = "reclame slide-info-right d-flex flex-column justify-content-baseline"
            }

            return  reclame.filter( reclame => reclame.disabled ).map( reclame => {
                return (
                    <div key={reclame.id} className={clName}>
                        <h4>{reclame.name}</h4>
                        <h2>{reclame.date}</h2>
                        <div className="registration">
                            <button className="draw in-block" onClick={this.showRegBlock} type="button">Записаться</button>
                        </div>
                    </div>
                )
            });
        }

    }

    checkWidth1() {

        if (window.innerWidth <= 768 ) {
            return 'block d-flex flex-column header-title-max-768 align-items-start justify-content-between'
        } else {
            return 'block d-flex flex-row align-items-start justify-content-between'
        }
    }

    checkWidth2() {
        if (window.innerWidth > 768) {
            return 'slide-info-left d-flex flex-column justify-content-center'
        } else {
            return 'slide-info-left align-items-center d-flex flex-column justify-content-center'
        }
    }

    render () {
        return (
            <div className="container d-flex flex-row justify-content-between">
                <div className={this.checkWidth1()} >
                    <div className={this.checkWidth2()}>
                        <h1 dangerouslySetInnerHTML={{__html: this.props[(this.props.page)].name}} />
                        <p>{this.props[(this.props.page)].desc}</p>
                    </div>
                    {this.showReclameBlock(this.props[(this.props.page)].reclame)}
                </div>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo,
        abonement: state.abonement,
        direction: state.direction,
        teachers: state.teachers,
        stock: state.stock,
        feedback: state.feedback
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showReg: (boolean) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean})
        }
    }))(HeadersTitleList);
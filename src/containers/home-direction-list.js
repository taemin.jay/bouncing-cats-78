import React, {Component} from 'react';
import {connect} from 'react-redux';

class HomeDirectionList extends Component {
    doublDigit(index) {
        return "0"+index
    }

    showDirInfo = (index) => {
        console.log(index)
        this.props.showInfo(!this.props.direction.directions.find(x => x.id === index).show, index)
    }

    showList () {
        let i = 0
        return this.props.direction.directions.filter( direction => direction.disabled ).map( direction => {
            i++
            return (
                <li key={direction.id} onClick={() => this.showDirInfo(direction.id)} className="home-direction-li mb-100">
                    <h2>{this.doublDigit(i)}</h2>
                    <h3>{direction.name}</h3>
                    <p>{direction.desc}</p>
                </li>
            )

        });
    }
    render () {
        return (
          <ul className="d-flex flex-row align-items-baseline justify-content-between flex-wrap mt-100">
              {this.showList()}
          </ul>
        );
    }
}

function mapStateToProps (state) {
    return {
        direction: state.direction
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showInfo: (boolean, index) => {
            dispatch({type: 'SHOW_DIRBLOCK', payload: boolean, id: index})
        }
    }))(HomeDirectionList);
import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import {connect} from 'react-redux';

class RouterList extends Component {

    showTabMenu = () => {
        this.props.showMenu(!this.props.router.show)
    }

    showList() {
        return this.props.router.routers.filter(router => router.disabled).map(router => {
            return (
                <Link to={router.link} title={router.desc} key={router.id} onClick={this.showTabMenu}>{router.name}</Link>
            )
        })
    }

    showMenu (clName, show) {
        if (clName) {
            let classNames = `d-flex flex-column align-center justify-content-around ${clName}`
            return (
                <nav className={ show ? `${classNames} menu-mobile-show` : `${classNames}`}>
                    {this.showList()}
                    <button onClick={this.showTabMenu} className="registration-close-btn" type="button"><img alt='any text' src={`${this.props.schoolinfo.url}close-btn.svg`}/></button>
                </nav>
            )
        }
        return (
                <nav className="navigation d-flex flex-row align-baseline justify-content-around">
                    {this.showList()}
                </nav>
            )
        }

    render () {
        return this.showMenu(this.props.clName, this.props.show)
    }
}

function mapStateToProps (state) {
    return {
        router: state.router,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showMenu: (boolean) => {
            dispatch({type: 'SHOW_MENU', payload: boolean})
        }
    }))(RouterList);
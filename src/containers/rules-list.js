import React, {Component} from 'react';
import {connect} from 'react-redux';

class RulesList extends Component {

    showList () {
        return this.props.abonements.rules.map( (rule,index) => {
            return (
                <li key={index}>
                    <p>{rule}</p>
                </li>
            )})
    }

    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2>Правила</h2>
                <ul className="d-flex flex-row align-items-start justify-content-between flex-wrap">
                    {this.showList()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        abonements: state.abonement
    };
}

export default connect(mapStateToProps)(RulesList);
import React, {Component} from 'react';
import {connect} from 'react-redux';

class SheduleHeaderList extends Component {

    showLessonsList () {
        let lessons = this.props.schedule.type.find(type => type.name==="lesson")
        if (lessons.disabled) {
            return lessons.list.filter( lesson => lesson.disabled ).map(lesson => {
                return (
                    <li key={lesson.id}>
                        <div className="schedule-color-point" style={{content: "", backgroundColor: lesson.color}} />
                        <h5>{lesson.name}</h5>
                    </li>
                )
            })

        }
    }

    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h3>{this.props.schedule.name}</h3>
                <ul className="d-flex flex-row flex-wrap justify-content-between">
                    {this.showLessonsList()}
                </ul>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        schedule: state.schedule
    };
}

export default connect(mapStateToProps)(SheduleHeaderList);
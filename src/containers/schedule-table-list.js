import React, {Component} from 'react';
import {connect} from 'react-redux';

class SheduleTableList extends Component {

    convertTime (houre, schedule_day) {
        if (schedule_day && schedule_day.minutes && schedule_day.minutes !== '') {
            return houre+":"+schedule_day.minutes
        }

        return houre+":00"
    }

    showRegBlock = (schedule_day, day, houre) => {
        if (schedule_day.type !== "team") {
            let additional = {
                name: schedule_day.name,
                day: day[2],
                time: this.convertTime(houre, schedule_day)
            }
            this.props.showReg(!this.props.feedback.feedbacks.find(x => x.page === "registration").show, additional)
        }
    }

    getColor (type, name) {
        let schedule_type = this.props.schedule.type.find(x => x.name === type),
            schedule_type_name = schedule_type.list.find(x => x.name === name)
        return schedule_type_name.color
    }

    checkEnableTeacher (teacher) {
        if (teacher) {
            return this.props.teachers.list.find(x => x.name === teacher).disabled
        } else if (teacher === "") {
            return true
        } else {
            return false
        }
    }

    checkEnableLessonName (type, name) {
        console.log(type, name)
        let schedule_type = this.props.schedule.type.find(x => x.name === type)
        return schedule_type.list.find(x => x.name === name).disabled
    }

    checkEnableType (type) {
        let schedule_type = this.props.schedule.type.find(x => x.name === type)
        return schedule_type.disabled
    }

    registrationButton (type) {
        if (type === "lesson") {
            return (
                <div className="lesson-registration">
                    <svg className="lesson-registration-svg"
                         viewBox="0 0 576 512">
                        <path fill="currentColor"
                              d="M417.8 315.5l20-20c3.8-3.8 10.2-1.1 10.2 4.2V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h292.3c5.3 0 8 6.5 4.2 10.2l-20 20c-1.1 1.1-2.7 1.8-4.2 1.8H48c-8.8 0-16 7.2-16 16v352c0 8.8 7.2 16 16 16h352c8.8 0 16-7.2 16-16V319.7c0-1.6.6-3.1 1.8-4.2zm145.9-191.2L251.2 436.8l-99.9 11.1c-13.4 1.5-24.7-9.8-23.2-23.2l11.1-99.9L451.7 12.3c16.4-16.4 43-16.4 59.4 0l52.6 52.6c16.4 16.4 16.4 43 0 59.4zm-93.6 48.4L403.4 106 169.8 339.5l-8.3 75.1 75.1-8.3 233.5-233.6zm71-85.2l-52.6-52.6c-3.8-3.8-10.2-4-14.1 0L426 83.3l66.7 66.7 48.4-48.4c3.9-3.8 3.9-10.2 0-14.1z"
                              ></path>
                    </svg>
                </div>
            )
        }
        return null
    }

    checkSchedule (day, houre) {
        let schedule = this.props.schedule,
            schedule_day = schedule.schedules[day[1]].filter(day => day.time === houre),
            buff = []
        console.log(schedule_day.length)
        schedule_day.forEach( schedule_day => {
            if (schedule_day && this.checkEnableType(schedule_day.type) && this.checkEnableLessonName(schedule_day.type, schedule_day.name) && this.checkEnableTeacher(schedule_day.teacher)) {
                let styles = {
                    backgroundColor: this.getColor(schedule_day.type, schedule_day.name)
                }
                buff.push(
                    <div key={day[1] + "-" + houre} onClick={() => this.showRegBlock(schedule_day, day, houre)}
                         className={`table-schedule ${schedule_day.type}`} style={styles}>
                        <h6>{this.convertTime(houre, schedule_day)}</h6>
                        <h6>{schedule_day.name}</h6>
                        <p>{schedule_day.place}</p>
                        <p>{schedule_day.teacher}</p>
                        {this.registrationButton(schedule_day.type)}
                    </div>
                )
            }
        })
        /*if (buff.length === 1) {
            return buff
        } else if (buff.length > 1) {
            return <div className="none-style d-flex flex-column align-items-center justify-content-between">
                    {buff}
                </div>
        } else {
            return <div key={day[1] + "-" + houre} className="d-flex align-items-center">
                <h5>{this.convertTime(houre, schedule_day)}</h5></div>
        }*/

        if (buff.length > 0) {
            return buff
        }
        return <div key={day[1] + "-" + houre} className="d-flex align-items-center">
            <h5>{this.convertTime(houre, schedule_day)}</h5></div>

    }

    showShedule (day) {
        let array = []
        for (let i=this.props.schedule.time_start; i <= this.props.schedule.time_end; i++ ) {
            array.push(this.checkSchedule(day, i))
        }
        return array
    }

    showTable () {
        return this.props.schedule.dayofweek.map( (day, index) => {
            return (
                <div key={index} className="column">
                    <div className="table-header" >
                        <h4>{day[0]}</h4>
                    </div>
                    {this.showShedule(day)}
                </div>
            )
        })
    }

    render () {
        return (
            <div className="block">
                <div className="table d-flex flex-row flex-wrap">
                    {this.showTable()}
                </div>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        schedule: state.schedule,
        teachers: state.teachers,
        feedback: state.feedback
    };
}

export default connect(mapStateToProps,
    dispatch => ({
        showReg: (boolean, additional) => {
            dispatch({type: 'SHOW_REGBLOCK', payload: boolean, additional: additional})
        }
    }))(SheduleTableList);
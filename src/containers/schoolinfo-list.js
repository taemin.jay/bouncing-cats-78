import React, {Component} from 'react';
import {connect} from 'react-redux';


class ShoolInfoList extends Component {

    showP (info) {
        return info.map( (info, index) => {
            return (<p key={index}>{info}</p>)
        })

    }

    showList () {
        return this.props.schoolinfo.benifit.filter( schoolinfo => schoolinfo.disabled).map( (schoolinfo,index) => {
            if (index === 0 || index%2 === 0) {
                return (
                    <li key={schoolinfo.id} className="d-flex flex-row flex-wrap align-items-center justify-content-between mb-100">
                        <div className="img-rectangle d-flex align-items-center">
                            <div className="rectangle-left"><img src={`${this.props.schoolinfo.url}rectangle.svg`} alt=""/></div>
                            <img className="img-schoolinfo" src={`${this.props.schoolinfo.url}${schoolinfo.img}`} alt=""/>
                        </div>
                        <div className="benefit-schoolinfo">
                            <div className="d-flex flex-column benefit-right">
                                <h3>{schoolinfo.name}</h3>
                                {this.showP(schoolinfo.desc)}
                            </div>
                        </div>
                    </li>
                )
            } else {
                return (
                    <li key={schoolinfo.id} className="d-flex flex-row-reverse flex-wrap align-items-center justify-content-between mb-100">
                        <div className="img-rectangle d-flex align-items-center">
                            <div className="rectangle-right"><img src={`${this.props.schoolinfo.url}rectangle.svg`} alt=""/></div>
                            <img className="img-schoolinfo" src={`${this.props.schoolinfo.url}${schoolinfo.img}`} alt=""/>
                        </div>
                        <div className="benefit-schoolinfo">
                            <div className="d-flex flex-column benefit-left">
                                <h3>{schoolinfo.name}</h3>
                                {this.showP(schoolinfo.desc)}
                            </div>
                        </div>
                    </li>
                )
            }

        });
    }
    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2> {this.props.schoolinfo.name2} - это: </h2>
                <ul className="mt-100">
                    {this.showList()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(ShoolInfoList);
import React, {Component} from 'react';
import {connect} from 'react-redux';
import Sliders from '../actions/Slider/Sliders'

class StocksList extends Component {
    listPrepareStock (t) {
        return t
            .filter( stock => stock.disabled)
            .map(stock => {return {
                name: stock.name,
                desc: stock.desc,
                event: stock.event,
                button: "",
                img: `${this.props.schoolinfo.url}${stock.img}`
            }})
    }

    showTitle (page) {
        if (page) {
            return this.props.stocks.name
        }
    }

    render () {
        return (
            <div className="block d-flex flex-column justify-content-between">
                <h2>{this.showTitle(this.props.page)}</h2>
                <Sliders timer={{name:"stockTimer", delay: 7000}} list={this.listPrepareStock(this.props.stocks.list)} clNameSlide="slider-stock" decor="deckor-contain-stocks"/>
            </div>
        );
    }
}

function mapStateToProps (state) {
    return {
        stocks: state.stocks,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps)(StocksList);
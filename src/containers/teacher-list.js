import React, {Component} from 'react';
import {connect} from 'react-redux';

class TeachersList extends Component {

    showTeachInfo = (index) => {
        this.props.showInfo(!this.props.teachers.list.find(x => x.id === index).show, index)
    }

    showList () {
        return this.props.teachers.list.filter( teacher => teacher.disabled).map( (teacher,index) => {

            let arrow = `${this.props.schoolinfo.url}arrowRight-moreinfo.svg`,
                img =`${this.props.schoolinfo.url}${teacher.badge}`

            if (index === 0 || index%2 === 0) {
                return (
                    <li key={teacher.id} onClick={() => this.showTeachInfo(teacher.id)} className="d-flex flex-column align-items-center justify-content-between mt-100">
                        <div className="d-flex flex-column align-items-baseline">
                            <div className="block-teacher-img">
                                <img className="img-teacher" src={img} alt=""/>
                                <img className="arrow-moreinfo" src={arrow} alt=""/>
                            </div>
                            <h3>{teacher.name}</h3>
                            <h4>{teacher.styles.join(", ")}</h4>
                            <p>{teacher.desc}</p>
                        </div>
                    </li>
                )
            } else {
                return (
                    <li key={teacher.id} onClick={() => this.showTeachInfo(teacher.id)} className="d-flex flex-column align-items-center justify-content-between mt-300">
                        <div className="d-flex flex-column align-items-baseline">
                            <div className="block-teacher-img">
                                <img className="img-teacher" src={img} alt=""/>
                                <img className="arrow-moreinfo" src={arrow} alt=""/>
                            </div>
                            <h3>{teacher.name}</h3>
                            <h4>{teacher.styles.join(", ")}</h4>
                            <p>{teacher.desc}</p>
                        </div>
                    </li>
                )
            }

        });
    }

    render () {
        return (
            <ul className="block d-flex flex-row flex-wrap justify-content-between">
                {this.showList()}
            </ul>
        );
    }
}

function mapStateToProps (state) {
    return {
        teachers: state.teachers,
        schoolinfo: state.schoolinfo
    };
}

export default connect(mapStateToProps,
        dispatch => ({
            showInfo: (boolean, index) => {
                dispatch({type: 'SHOW_INFOBLOCK', payload: boolean, id: index})
            }
        }))(TeachersList);
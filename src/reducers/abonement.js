export default function () {
    return {
        disabled: true,
        id: 1,
        name_home: "Абонементы</br>и Акции",
        name: "Абонементы и Правила",
        name2: "Абонементы",
        desc: "Мы предоставляем максимально высокое качество обучение по демократичным ценам. Запишитесь к нам на пробное занятие и убедитесь в этом сами!",
        ad: "При оплате за 3 месяца — экономия 10%",
        btnName_default: "Купить",
        reclame: [
            {
                disabled: true,
                id: 1,
                name: "Запишись на пробное занятие",
                date: "",
                btnName: "Записаться"
            }
        ],
        list: [
            {
                disabled: false,
                home_page: false,
                id: 1,
                name: "Детская группа",
                price: "2800",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "8 занятий 2 раза в неделю.",
                    "Свободное посещение Джемов.",
                    "Только для детей 7-12 лет."
                ],
                btnName: null,
                reclame: false

            },
            {
                disabled: true,
                home_page: false,
                id: 2,
                name: "8 занятий",
                price: "3100",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "8 занятий 2 раза в неделю.",
                    "Свободное посещение Джемов.",
                    "Действует 1 месяц."
                ],
                btnName: null,
                reclame: false
            },
            {
                disabled: true,
                home_page: true,
                id: 3,
                name: "16 занятий",
                price: "4300",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "16 занятий 4 раза в неделю.",
                    "Свободное посещение Джемов.",
                    "Абонемент действует в течение одного месяца."
                ],
                btnName: null,
                reclame: false
            },
            {
                disabled: true,
                home_page: true,
                id: 4,
                name: "Безлимитный",
                price: "5500",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "Неограниченное количество занятий.",
                    "Свободное посещение Джемов.",
                    "Абонемент действует в течение одного месяца."
                ],
                btnName: null,
                reclame: false
            },
            {
                disabled: true,
                home_page: false,
                id: 5,
                name: "Разовый",
                price: "500",
                price_sale: false,
                payment: "руб.",
                options: [],
                btnName: null,
                reclame: false
            },
            {
                disabled: true,
                home_page: true,
                id: 6,
                name: "Индивидуальный",
                price: "1200",
                price_sale: false,
                payment: "руб.",
                options: [
                    "Тренировка один на один с любым преподавателем."
                ],
                btnName: null,
                reclame: "5 индивидуальных занятий 4500руб"
            },
            {
                disabled: true,
                home_page: false,
                id: 7,
                name: "Only Choreo",
                price: "2300",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "4 занятия по 1,5 часа.",
                    "Свободное посещение Джемов.",
                    "Действует 1 месяц."
                ],
                btnName: null,
                reclame: false
            },
            {
                disabled: true,
                home_page: false,
                id: 8,
                name: "Курс IT's ALL about the FUNK",
                price: "3800",
                price_sale: false,
                payment: "руб./мес.",
                options: [
                    "12 часов 2 раза в неделю.",
                    "10** часов практики",
                    "Свободное посещение Джемов."
                ],
                btnName: null,
                reclame: false
            },
        ],
        rules: [
            "Первое занятие необходимо посетить в течение недели после покупки. В противном случае абонемент начинает действовать автоматически.",
            "Абонемент продлевается только в случае предоставления официальных документов подтверждающих болезнь, " +
            "билетов на самолёты/поезда(в случае командировки/отпуска).",
            "При утере абонемента его необходимо восстановить.",
            "Деньги за приобретённый абонемент не возвращаются.",
            "В дни, когда школа не работает, срок действия абонемента продлевается автоматически.",
            "При каждом посещении школы необходимо предъявить его администратору перед занятием.",
            "Одним абонементом может пользоваться только один клиент школы."
        ]
    }
}
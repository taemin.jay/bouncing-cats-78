export default function () {
    return {
        disabled: true,
        id: 1,
        name: "Наши</br>достижения",
        desc: "",
        achiev: [
            {
                disabled: true,
                id: 1,
                name: "Команда Da Fellows",
                regards: [
                    "3 место, Start Dance"
                ],
                desc: "Adults PRO",
                img: "achievements_8.jpg"
            },
            {
                disabled: true,
                id: 2,
                name: "Никита Цеплухин",
                regards: [
                    "Победитель Home Battle, СПб"
                ],
                desc: "hip-hop 1x1, beginners",
                img: "achievements_1.jpg"
            },
            {
                disabled: true,
                id: 3,
                name: "Арина Крючкова",
                regards: [
                    "Победительница UDO-battle, СПб"
                ],
                desc: "locking 1x1, beginners",
                img: "achievements_2.jpg"
            },
            {
                disabled: true,
                id: 4,
                name: "Елизавета Махова",
                regards: [
                    "Победительница Home Battle, СПб"
                ],
                desc: "waacking 1x1",
                img: "achievements_3.jpg"
            },
            {
                disabled: true,
                id: 5,
                name: "TTP crew",
                regards: [
                    "Победители KOD Russia, Москва"
                ],
                desc: "locking 4x4",
                img: "achievements_4.jpg"
            },
            {
                disabled: true,
                id: 6,
                name: "Максим Лятуринский",
                regards: [
                    "Победитель Home Battle All In, СПб"
                ],
                desc: "locking 1x1",
                img: "achievements_5.jpg"
            },
            {
                disabled: true,
                id: 7,
                name: "Power crew",
                regards: [
                    "2 место UDO, СПб"
                ],
                desc: "номинация beginners",
                img: "achievements_6.jpg"
            },
            {
                disabled: true,
                id: 8,
                name: "Семен Никандров",
                regards: [
                    "Победитель M-battle PRO, СПб"
                ],
                desc: "locking 1x1",
                img: "achievements_7.jpg"
            },
            {
                disabled: true,
                id: 9,
                name: "Команда That's The Point",
                regards: [
                    "2 место, Street Summit 2019"
                ],
                desc: "Best Dance Show",
                img: "achievements_9.jpg"
            }
        ]
    }
}
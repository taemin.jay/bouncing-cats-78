const initialState = {
        disabled: true,
        id: 1,
        name: "Обратная связь",
        desc: "",
        additional: null,
        forms: [
            {
                id: 1,
                field: "name",
                label: "Имя",
                disabled: true,
                required: true,
                pattern: ""
            },
            {
                id: 2,
                field: "telephone",
                label: "Телефон",
                disabled: true,
                required: true,
                pattern: ""
            }
        ],
        feedbacks: [
            {
                disabled: true,
                id: 1,
                page: "children",
                name: "Приходите знакомиться",
                desc: "Мы предоставляем максимально удобное расписание детских групп для того, чтобы было максимально удобно посещать занятия!",
                button: "Записаться на пробное занятие",
                forms: [
                    {
                        id: 3,
                        field: "email",
                        label: "E-mail",
                        disabled: true,
                        required: false,
                        pattern: ""
                    }
                ]
            },
            {
                disabled: true,
                id: 2,
                page: "contact",
                name: "Пишите нам",
                desc: "",
                button: "Отправить",
                forms: [
                    {
                        id: 4,
                        field: "title",
                        label: "Заголовок",
                        disabled: false,
                        required: false,
                        pattern: ""
                    },
                    {
                        id: 5,
                        field: "message",
                        label: "Сообщение",
                        disabled: false,
                        required: false,
                        pattern: ""
                    }
                ]
            },
            {
                disabled: true,
                show: false,
                id: 3,
                page: "registration",
                name: "Приходите знакомиться",
                desc: "",
                button: "Записаться",
                forms: []
            },
            {
                disabled: true,
                id: 4,
                page: "abonement",
                name: "Приходите знакомиться",
                desc: "Мы предоставляем максимально высокое качество обучение по демократичным ценам. Запишитесь к нам на пробное занятие и убедитесь в этом сами!",
                button: "Записаться на пробное занятие",
                forms: [
                    {
                        id: 3,
                        field: "email",
                        label: "E-mail",
                        disabled: true,
                        required: false,
                        pattern: ""
                    }
                ]
            }
        ]
    }

export default function (state = initialState, action) {
        switch (action.type) {
            case "SHOW_REGBLOCK":
                state.feedbacks.find(x => x.page === "registration").show = action.payload
                if (action.additional) {
                    state.additional = action.additional
                } else {
                    state.additional = null
                }
                return {...state}
            default:
                return state
        }
}
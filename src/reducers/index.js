import {combineReducers} from 'redux';
import DirectionsReducers from './direction';
import SchoolInfoReducers from './schoolinfo';
import AchievementsReducers from './achievements';
import RouterReducers from './router';
import AbonementsReducers from './abonement';
import StocksReducers from './stock';
import GalleryReducers from './gallery';
import TeachersReducers from './teacher';
import SchedulsReducers from './schedule';
import FeedbackReducers from './feedback';

const allReducers = combineReducers({
    router: RouterReducers,
    direction: DirectionsReducers,
    schoolinfo: SchoolInfoReducers,
    achievements: AchievementsReducers,
    abonement: AbonementsReducers,
    stocks: StocksReducers,
    gallery: GalleryReducers,
    teachers: TeachersReducers,
    schedule: SchedulsReducers,
    feedback: FeedbackReducers
});

export default allReducers
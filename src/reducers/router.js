const initialState = {
    disabled: true,
    show: false,
    id: 1,
    name: "",
    desc: "",
    additional: null,
    routers: [
        {
            disabled: false,
            id: 1,
            name: "Дети",
            desc: "Детская группа",
            link: "/children"
        },
        {
            disabled: true,
            id: 2,
            name: "Педагоги",
            desc: "Преподаватели студии",
            link: "/teacher"
        },
        {
            disabled: true,
            id: 3,
            name: "Расписание",
            desc: "Расписание",
            link: "/schedule"
        },
        {
            disabled: true,
            id: 4,
            name: "Направления",
            desc: "Направления обучения",
            link: "/direction"
        },
        {
            disabled: true,
            id: 5,
            name: "Цены",
            desc: "Абонементы и правила",
            link: "/price"
        },
        {
            disabled: true,
            id: 6,
            name: "Галерея",
            desc: "Галерея",
            link: "/gallery"
        },
        {
            disabled: false,
            id: 7,
            name: "Мероприятия",
            desc: "Лагерь и вечеринки",
            link: "/event"
        },
        {
            disabled: true,
            id: 8,
            name: "Контакты",
            desc: "Как с нами связаться",
            link: "/contact"
        }
    ]
}

export default function (state = initialState, action) {
    switch (action.type) {
        case "SHOW_MENU":
            state.show = action.payload
            return {...state}
        default:
            return state
    }
}
export default function () {
    return {
        disabled: true,
        id: 1,
        name: "Расписание",
        desc: "",
        time_start: "15",
        time_end: "21",
        dayofweek: [
            ["ПН", "MO", "Понедельник"],
            ["ВТ", "TU", "Вторник"],
            ["СР", "WE", "Среда"],
            ["ЧТ", "TH", "Четверг"],
            ["ПТ", "FR", "Пятница"],
            ["СБ", "SA", "Суббота"],
            ["ВС", "SU", "Воскресение"]
        ],
        type: [
            {
                disabled: true,
                id: 1,
                name: "lesson",
                list: [
                    {
                        disabled: false,
                        id: 1,
                        name: "kids",
                        color: "#F39200"
                    },
                    {
                        disabled: false,
                        id: 2,
                        name: "popping",
                        color: "#B2040B"
                    },
                    {
                        disabled: true,
                        id: 3,
                        name: "hip-hop",
                        color: "#0E14A2"
                    },
                    {
                        disabled: true,
                        id: 4,
                        name: "choreo",
                        color: "#00A19A"
                    },
                    {
                        disabled: true,
                        id: 5,
                        name: "locking",
                        color: "#148C00"
                    },
                    {
                        disabled: false,
                        id: 6,
                        name: "locking(курс)",
                        color: "#05bd17"
                    },
                    {
                        disabled: true,
                        id: 7,
                        name: "waacking",
                        color: "#ED00AB"
                    },
                    {
                        disabled: false,
                        id: 8,
                        name: "house",
                        color: "#824600"
                    }
                ],
            },
            {
                disabled: true,
                id: 2,
                name: "team",
                list: [
                    {
                        disabled: true,
                        id: 1,
                        name: "TTP",
                        color: "#F39200"
                    }
                ]
            },
            {
                disabled: false,
                id: 3,
                name: "jam",
                list: [
                    {
                        disabled: true,
                        id: 1,
                        name: "hip-hop",
                        color: "#F39200"
                    },
                    {
                        disabled: true,
                        id: 2,
                        name: "locking",
                        color: "#F39200"
                    },
                    {
                        disabled: true,
                        id: 3,
                        name: "waacking",
                        color: "#F39200"
                    },
                    {
                        disabled: true,
                        id: 4,
                        name: "house",
                        color: "#F39200"
                    }
                ]
            },
        ],
        schedules: {
            MO: [
                {
                    time: 19,
                    type: "lesson",
                    name: "hip-hop",
                    teacher: "Никандрова Александра",
                    place: "зал 10"
                },
                {
                    time: 19,
                    type: "lesson",
                    name: "locking(курс)",
                    teacher: "Никандров Семен",
                    place: "зал 7"
                },
                {
                    time: 20,
                    type: "lesson",
                    name: "locking",
                    teacher: "Никандров Семен",
                    place: "зал 10"
                }
            ],
            TU: [
                {
                    time: 19,
                    minutes: 30,
                    type: "lesson",
                    name: "waacking",
                    teacher: "Махова Елизавета",
                    place: "зал 10"
                }
            ],
            WE: [
                {
                    time: 20,
                    type: "lesson",
                    name: "hip-hop",
                    teacher: "Никандрова Александра",
                    place: "зал 10"
                },
                {
                    time: 21,
                    type: "lesson",
                    name: "choreo",
                    teacher: "Никандрова Александра",
                    place: "зал 10"
                }
            ],
            TH: [],
            FR: [
                {
                    time: 19,
                    type: "lesson",
                    name: "waacking",
                    teacher: "Махова Елизавета",
                    place: "зал 10"
                },
                {
                    time: 19,
                    type: "lesson",
                    name: "locking(курс)",
                    teacher: "Никандров Семен",
                    place: "зал 7"
                },
                {
                    time: 20,
                    type: "lesson",
                    name: "locking",
                    teacher: "Никандров Семен",
                    place: "зал 10"
                },
                {
                    time: 21,
                    minutes: 30,
                    type: "team",
                    name: "TTP",
                    teacher: ""
                }
            ],
            SA: [],
            SU: [
                {
                    time: 21,
                    minutes: 30,
                    type: "team",
                    name: "TTP",
                    teacher: ""
                }
            ]
        }
    }
}

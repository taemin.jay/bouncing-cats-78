export default function () {
    return {
        disabled: true,
        id: 1,
        name: "Школа</br>Уличных</br>Танцев",
        name2: "Bouncing Cats",
        url: "https://bouncingcats78.ru/",
        desc: "Школа, в которой обучают популярным в наше время уличным танцевальным направлениям. " +
        "Мы готовы познакомить каждого с этой культурой в самом центре Петербурга.",
        contacts: {
            id: 1,
            name: "Контакты",
            address: {
                name: "Санкт-Петербург, Чкаловский пpоспект 15",
                desc: "2-3 минуты от м.Чкаловская прямо по проспекту, вход через двери магазина Перекресток." +
                " Слева напротив касс - лестница. Поднимайтесь на 4 этаж. За большой черной дверью будет сидеть администратор.",
                map: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1997.0918802139515!2d30.29359001598234!3d59.96379936703082!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4696314ed99f7d5d%3A0xdbfd85e0ec8eab0a!2z0KfQutCw0LvQvtCy0YHQutC40Lkg0L_RgC4sIDE1LCDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywgMTk3MTEw!5e0!3m2!1sru!2sru!4v1538039948390"
            },
            telephone: [
                "908-15-25",
                "+78129081525"
            ],
            email: "bouncingcats78@gmail.com"
        },
        reclame: [
            {
                disabled: false,
                id: 1,
                name: "Открытый урок в группу по Locking",
                date: "29 марта",
                btnName: "Записаться"
            }
        ],
        social: [
            {
                disabled: true,
                mainScreenDisabled: true,
                id: 1,
                name: "Группа ВК",
                iconMainScreen: "vk_header.svg",
                icon:"vk.svg",
                url: "https://vk.com/bouncingcats78"
            },
            {
                disabled: true,
                mainScreenDisabled: true,
                id: 2,
                name: "Инстаграм",
                iconMainScreen: "instagram_header.svg",
                icon: "instagram.svg",
                url: "https://www.instagram.com/bouncingcats78"
            },
            {
                disabled: false,
                mainScreenDisabled: false,
                id: 3,
                name: "facebook",
                iconMainScreen: "",
                icon: "facebook.svg",
                url: "https://www.instagram.com/bouncingcats78"
            },
            {
                disabled: false,
                mainScreenDisabled: false,
                id: 4,
                name: "ok",
                iconMainScreen: "",
                icon: "",
                url: ""
            }
        ],
        benifit: [
            {
                disabled: true,
                id: 1,
                name: "Знакомство с культурой",
                desc: [
                    "Обучение в Bouncing Cats — это полноценное погружение в атмосферу и культуру танца. ",
                "Кроме движений и базовых шагов самых популярных уличных стилей, мы дадим понять и почувствовать " +
                    "суть каждого танцевального направления, " +
                "причины формирования определенного характера и темперамента танца."
                ],
                img: "benefit_1.jpg"
            },
            {
                disabled: true,
                id: 2,
                name: "Качественное обучение",
                desc: [
                    "Тренировки основаны на взаимопонимании ученика и тренера. " +
                "Мы с уважением относимся к учащимся в нашей школе, " +
                "и учитываем особенности каждого.",
                    "Основываясь на многолетнем опыте, тренер выявляет и развивает творческие способности " +
                "ученика, вырабатывая индивидуальный подход для раскрытия его потенциала.",
                "Особое внимание уделяется базовым элементам, ритмике и характеру в танце. " +
                "Мы помогаем развить образное мышление, память и трудолюбие, " +
                "привить интерес к спорту и здоровому образу жизни."
                ],
                img: "benefit_2.jpg"
            },
            {
                disabled: true,
                id: 3,
                name: "Неограниченные возможности",
                desc: [
                    "Цель нашей школы не просто обучить людей танцевальной технике, " +
                "но и наполнить их жизнь радостью, новым смыслом и вдохновением.",
                "Танцевальные вечеринки и соревнования - это те мероприятия, ради которых мы и изучаем примудрости танцев. " +
                "Регулярные занятия в группах, индивидуальные занятия с преподавателями, поездки на мастер-классы, и даже домашняя работа - " +
                "все это необходимо для развития и продвижения к высоким целям."
                ],
                img: "benefit_3.jpg"
            }
        ],
        more_benifit: {
            disabled: true,
            id: 1,
            name: "Дополнительные</br>преимущества",
            list: [
                {
                    disabled: true,
                    id: 1,
                    name: "Для новичков и начинающих",
                    desc: "Мы находим подход к каждому ченику нашей школы. А достижения текущих вы можете оценить сами",
                    img: "more_benifit_1.svg"
                },
                {
                    disabled: true,
                    id: 2,
                    name: "Удобное расположение",
                    desc: "Мы находимся в 5 минутах ходьбы от ст.м.Чкаловская",
                    img: "more_benifit_2.svg"
                },
                {
                    disabled: true,
                    id: 3,
                    name: "Низкие цены при высоком качестве",
                    desc: "И это не шутка. Сравните с конкурентами",
                    img: "more_benifit_3.svg"
                }
            ]
        }
    }
}
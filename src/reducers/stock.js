export default function () {
    return {
        disabled: true,
        id: 1,
        name: "Акции",
        desc: "",
        list: [
            {
                disabled: false,
                id: 3,
                name: "SALE",
                event: "20%",
                desc: "Только 29.11.2019",
                img: "stock_3_bf.jpg"
            },
            {
                disabled: true,
                id: 1,
                name: "Акция",
                event: "",
                desc: "",
                img: "stock_1.jpg"
            },
            {
                disabled: true,
                id: 2,
                name: "SALE",
                event: "15%",
                desc: "В школе Bouncing Cats уроки проводят одни из лучших танцоров Санкт-Петербурга",
                img: "stock_2.jpg"
            }
        ]
    }
}

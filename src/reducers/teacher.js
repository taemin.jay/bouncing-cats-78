const initialState = {
        disabled: true,
        id: 1,
        name: "Команда</br>профессионалов",
        desc: "В нашей школе работают признанные мастера своего дела. " +
        "Они все имеют многолетний опыт работы, любят всей душой свои направления и готовы открыто и продуктивно общаться.",
        list: [
            {
                disabled: true,
                show: false,
                id: 1,
                name: "Никандров Семен",
                badge: "semen_badge.jpg",
                position: [
                    "Руководитель студии",
                    "Педагог"
                ],
                styles: [
                    "Locking",
                    "Choreo"
                ],
                desc: "Сильнейший представитель фанкового стиля Петербурга - Семён Никандров - уже многие годы воспитывает новое поколение локеров.",
                info: [
                    "Сильнейший представитель фанкового стиля Петербурга - Семён Никандров - уже многие годы воспитывает новое поколение локеров." +
                    " Настоящий мастер своего дела и прекрасный педагог!",
                    "Cооснователь школы танцев Bouncing Cats. Участник команды That's The Point в составе которой является двухкратным победителем KOD Russia. " +
                    "В 2016 году That's The Point успешно дебютировали в Лос-Анжелесе(США) на KOD World Cup и в Париже(Франция) на Pay The Cost To Be The Boss.",
                    "Победитель многих командных чемпионатов и баттлов.(UDO2014 locking 1x1/crew advanced winner,love your dance2015 crew adv winner, " +
                    "Culture Street contest locking 1x1 winner, Fordanzo 2016 locking 1x1 winner, Mt fest2017 locking 1x1 winner,MIF fest2017 crew adv winner," +
                    " M battle2018 locking 1x1 winner...)"
                ],
                media: {
                    video: [{video: "Semen.mp4", poster: "semen_poster.jpg"}],
                    images: [
                        "Semen1.jpg",
                        "Semen2.jpg",
                        "Semen3.jpg",
                        "Semen4.jpg"
                    ]
                }
            },
            {
                disabled: true,
                show: false,
                id: 2,
                name: "Никандрова Александра",
                badge: "sasha_badge.jpg",
                position: [
                    "Руководитель студии",
                    "Педагог"
                ],
                styles: [
                    "Hip-Hop"
                ],
                desc: "Танцор, хореграф, профессиональный и отзывчивый педагог. Призер многих крупных танцевальных чемпионатов (KOD Russia, UDO, Love your dance, WOD и другие).",
                info: [
                    "Самый яркий и позитивный член нашего танцевального сообщества. " +
                    "Танцор, хореграф, профессиональный и отзывчивый педагог.",
                    "В течение тренировки Саша успевает не только дать подготовленный для своих учеников материал, но и уделить время каждому из них.",
                    "Призер многих крупных танцевальных чемпионатов (KOD Russia, UDO, Love your dance, WOD и другие)." +
                    "Александра является серябрянным призером чемпионата мира UDO world street dance championship Глазго (Шотландия). " +
                    "Участница команды Thats the Point. Со основатель школы танцев Bouncing Cats."
                ],
                media: {
                    video: [{video: "Sasha.mp4", poster: "sasha_poster.jpg"}],
                    images: [
                        "Sasha1.jpg",
                        "Sasha2.jpg",
                        "Sasha3.jpg",
                        "Sasha4.jpg"
                    ]
                }
            },
            {
                disabled: false,
                show: false,
                id: 3,
                name: "Изместьева Ольга",
                badge: "olga_badge.jpg",
                position: [
                    "Педагог"
                ],
                styles: [
                    "Popping"
                ],
                desc: "Яркий представитель Петербургского танцевального сообщества. За последние годы она собрала огромное количество информации о стиле паппинг.",
                info: [
                    "Яркий представитель Петербургского танцевального сообщества. За последние годы она собрала огромное количество информации о стиле паппинг. " +
                    "Посещала мастер-классы и фестивали по всему миру, и теперь она готова делиться полученными знаниями со своими учениками.",
                    "На собственном примере доказала, что упорство и работа над собой всегда приводят вас к поставленным целям." +
                    "По настоящему преданный своему делу танцор и педагог.",
                    "Участница команды Gun The Rozes. Танцевальный стаж более 10 лет." +
                    "Участие и победы в фестивалях: KOD Russia (Москва), United Dance Open (Санкт-Петербург), Summer Dance Forever (Амстердам), UDO World Championship (Глазго)."
                ],
                media: {
                    video: [{video: "Olga.mp4", poster: "olga_poster.jpg"}],
                    images: [
                        "Olga1.jpg",
                        "Olga2.jpg",
                        "Olga3.jpg",
                        "Olga4.jpg"
                    ]
                }
            },
            {
                disabled: false,
                show: false,
                id: 4,
                name: "BJ Slav",
                badge: "slav_badge.jpg",
                position: [
                    "Педагог"
                ],
                styles: [
                    "House"
                ],
                desc: "Уже больше 20 лет Слав несёт танцевальную культуру в массы, это выражается не только в уроках и мастер-классах, но и в мероприятиях которые он организует.",
                info: [
                    "Уже больше 20 лет Слава несёт танцевальную культуру в массы, это выражается не только в уроках и мастер-классах, но и в мероприятиях которые он организует.",
                    "Фестиваль \"Hip-Hop Антология\" каждый год даёт возможность жителям Петербурга знакомиться с культурой уличных танцев.",
                    "Организатор /резидент/ крупнейших фестивалей и чемпионатов Санкт-Петербурга Hip-Hop Антология (с 2008-…..), Северный Бит (2012,2014)," +
                    " Respect My Talent (2009-2014), United Dance Open (2012), Abzats Anniversary (2015), Street Summit (2016), Underground Session (2016), " +
                    "V1 Fest (2016), Rockin’ Star (2016), Иди Танцуй (2016-2017) Main Street Battle (2016-2017), Open Your Mind (2016-2017), Just Dance Fest (2016-2017), " +
                    "Jackin’ Session (2017) Участник команды \"Smart House\". Настоящий фанат своего дела, прекрасный человек."
                ],
                media: {
                    video: [{video: "Slav.mp4", poster: "slav_poster.jpg"}],
                    images: [
                        "Slav1.jpg",
                        "Slav2.jpg",
                        "Slav3.jpg",
                        "Slav4.jpg"
                    ]
                }
            },
            {
                disabled: true,
                show: false,
                id: 5,
                name: "Махова Елизавета",
                badge: "elizaveta_badge.jpg",
                position: [
                    "Педагог"
                ],
                styles: [
                    "Waacking"
                ],
                desc: "Элегантная и утонченная, настоящая «фанк-дива». Увлечённый и преданный своему делу человек.\nУчастница команды TTP crew.",
                info: [
                    "Элегантная и утонченная, настоящая «фанк-дива».",
                    "Увлечённый и преданный своему делу человек.",
                    "В процессе своего танцевального развития успела познакомиться с такими стилями как Hip-Hop, Locking, Popping, но именно Waacking покорил её сердце.",
                    "Участница команды TTP crew.",
                    "Участия и победы в баттлах:\n" +
                    "Top 4 на KOD Russia 2017 в составе команды TopLockStock, Real dance meeting locking 1x1 winner," +
                    "Christmas Vogue Ball 2017 waacking beg. 1x1 winner, Home battle 2018 waacking 1x1 winner x2, 1 place “House of Power” MIF 2015, 1 place “House of Power” Иди танцуй 2017, 1 place «That’s the point crew” Millennium Festival 2018..." +
                    "Посетила множество мастер-классов ведущих танцоров, таких как Rada Waackengers, Yoon Ji, Lip J, " +
                    "Ebony, Tyron Proctor, Taya Ihow, Chrissy Chou, Dashine Ihow и многих других."
                ],
                media: {
                    video: [{video:"Elizaveta.mp4", poster:"elizaveta_poster.jpeg"}],
                    images: [
                        "Elizaveta1.jpeg",
                        "Elizaveta2.jpeg",
                        "Elizaveta3.jpeg",
                        "Elizaveta4.jpeg"
                    ]
                }
            }

        ]
    }

export default function (state = initialState, action) {
    switch (action.type) {
        case "SHOW_INFOBLOCK":
            state.list.find(x => x.id === action.id).show = action.payload
            return {...state}
        default:
            return state
    }
}